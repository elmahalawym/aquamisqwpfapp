﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using System.Text.RegularExpressions;
using Microsoft.Reporting.WinForms;
using System.Data.Entity;

using MAIN.Models;
using MAIN.ViewModels;
using MAIN.Reports;

//using System.Windows.Forms;

namespace MAIN
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    /// 

    public partial class MainWindow : Window
    {

        ADDRESS add;
        CUSTOMER cus;
        INSTALLMENT ins;

        private FilterCompanyDataContext db = new FilterCompanyDataContext();

        int street_ID;

        public MainWindow()
        {
            InitializeComponent();

            this.Loaded += new RoutedEventHandler(MainWindow_Loaded);

            // assign data context

            placesTab.DataContext = new PlacesViewModel();
            addInstallmentTab.DataContext = new InstallmentsAddViewModel();
            installmentsSearchTab.DataContext = new InstallmentsSearchViewModel();
            addCustomerTab.DataContext = new AddCustomerViewModel();
            editCustomerTab.DataContext = new CustomerEditViewModel();
            customerSearchTab.DataContext = new CustomerSearchViewModel();
            maintenanceSearchTab.DataContext = new MaintenanceSearchViewModel();
            addItemTab.DataContext = new ItemsAddViewModel();
            searchItemsTab.DataContext = new ItemsSearchViewModel();
            itemsEditTab.DataContext = new ItemsEditViewModel();
            addMaintenanceInvoiceTab.DataContext = new MaintenanceAddInvoiceViewModel();
            maintenanceInvoiceSearchTab.DataContext = new MaintenanceInvoiceSearchViewModel();
            maintenanceInvoiceEditTab.DataContext = new MaintenanceEditInvoiceViewModel();
            incomingsTab.DataContext = new IncomingsViewModel();
            internaloutgoingAddTab.DataContext = new InternalOutgoingsAddViewModel();
            outgoingsSearchTab.DataContext = new OutgoingsSearchViewModel();
            internalOutgoingEditTab.DataContext = new InternalOutgoingsEditViewModel();
            externalOutgoingsAddTab.DataContext = new ExternalOutgoingsAddViewModel();
            externalOutgoingsEditTab.DataContext = new ExternalOutgoingsEditViewModel();

            //   rv_Street_Customers.Load += ReportViewer_Load;

            //add = new ADDRESS();
            //cus = new CUSTOMER();
            //ins = new INSTALLMENT();

            //Bind_cb_MAIN_REGION_ADD_CUSTOMER(cb_MAINREGION_ADD_CUSTOMER);

            //add.Bind_cb_REGION_WITH_TYPE(cb_ADDNEWMAINREGION_DELETE,3);

            //add.Bind_cb_REGION_WITH_TYPE(cb_DELETE_MAINREGION_IN_SUBREGION_ADDRESS, 3);


            //add.loadMainInSubRegionAddress(cb_GETMAINREGION_SUBREGION_ADDRESS);
            //add.Bind_cb_REGION_WITH_TYPE(cb_DELETE_MAINREGION_IN_SUBREGION_ADDRESS, 3);
            ////--------------------------------------------------------------------
            //add.Bind_cb_REGION_WITH_TYPE(cb_ADD_STREET_MAINREGION_ADDRESS, 3);


            //add.Bind_cb_REGION_WITH_TYPE(cb_DELETE_MAINREGION_IN_STREET_ADDRESS, 3);

            //Bind_cb_MAINTENANCE_RATING(cb_MAINTENANCE_RATING);

            //---------------------------------REPORTS--------------------------------------------------
            //add.Bind_cb_REGION_WITH_TYPE(cb_Region_Street_Regions_Reports, 1);


            // Bind_cb_STREET_ADD_CUSTOMER(cb_STREET_ADD_CUSTOMER);
            // Bind_cb_STREET_ADD_CUSTOMER(11);
        }

        void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            customerSearchTab.Focus();
        }

        public void Bind_cb_MAIN_REGION_ADD_CUSTOMER(ComboBox comboBoxName)
        {
            //  SqlConnection conn = new SqlConnection("Data Source=.\\SQLEXPRESS;AttachDbFilename=C:\\Users\\DELL\\Documents\\newd.mdf;Integrated Security=True;Connect Timeout=30;User Instance=True");
            string ConString = ConfigurationManager.ConnectionStrings["ConString"].ConnectionString;
            string CmdString = string.Empty;
            using (SqlConnection con = new SqlConnection(ConString))
            {
                CmdString = "SELECT ADDRESS_ID,NAME FROM ADDRESS WHERE TYPE = 3";
                SqlCommand cmd = new SqlCommand(CmdString, con);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds, "ADDRESS");
                comboBoxName.ItemsSource = ds.Tables[0].DefaultView;
                comboBoxName.DisplayMemberPath = ds.Tables[0].Columns["NAME"].ToString();
                comboBoxName.SelectedValuePath = ds.Tables[0].Columns["ADDRESS_ID"].ToString();

            }
        }

        public void Bind_cb_STREET_ADD_CUSTOMER(int value)
        {
            //  SqlConnection conn = new SqlConnection("Data Source=.\\SQLEXPRESS;AttachDbFilename=C:\\Users\\DELL\\Documents\\newd.mdf;Integrated Security=True;Connect Timeout=30;User Instance=True");
            string ConString = ConfigurationManager.ConnectionStrings["ConString"].ConnectionString;
            string CmdString = string.Empty;
            using (SqlConnection con = new SqlConnection(ConString))
            {
                // ComboBoxItem typeItem = (ComboBoxItem)cb_SUBREGION_ADD_CUSTOMER.SelectedItem;
                //  string value = cb_SUBREGION_ADD_CUSTOMER.Text.ToString().Trim();

                CmdString = "SELECT ADDRESS_ID,NAME FROM ADDRESS WHERE TYPE = 1 AND PARENT = " + value;
                SqlCommand cmd = new SqlCommand(CmdString, con);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds, "ADDRESS");
                cb_STREET_ADD_CUSTOMER.ItemsSource = ds.Tables[0].DefaultView;
                cb_STREET_ADD_CUSTOMER.DisplayMemberPath = ds.Tables[0].Columns["NAME"].ToString();
                cb_STREET_ADD_CUSTOMER.SelectedValuePath = ds.Tables[0].Columns["ADDRESS_ID"].ToString();

            }
        }


        public void Bind_cb_SUB_REGION_ADD_CUSTOMER(int value)
        {
            //  SqlConnection conn = new SqlConnection("Data Source=.\\SQLEXPRESS;AttachDbFilename=C:\\Users\\DELL\\Documents\\newd.mdf;Integrated Security=True;Connect Timeout=30;User Instance=True");
            string ConString = ConfigurationManager.ConnectionStrings["ConString"].ConnectionString;
            string CmdString = string.Empty;
            using (SqlConnection con = new SqlConnection(ConString))
            {
                CmdString = "SELECT ADDRESS_ID,NAME FROM ADDRESS WHERE TYPE = 2 AND PARENT = " + value;
                SqlCommand cmd = new SqlCommand(CmdString, con);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds, "ADDRESS");
                cb_SUBREGION_ADD_CUSTOMER.ItemsSource = ds.Tables[0].DefaultView;
                cb_SUBREGION_ADD_CUSTOMER.DisplayMemberPath = ds.Tables[0].Columns["NAME"].ToString();
                cb_SUBREGION_ADD_CUSTOMER.SelectedValuePath = ds.Tables[0].Columns["ADDRESS_ID"].ToString();

                //cb_SUBREGION_ADD_CUSTOMER.SelectionChanged += new SelectionChangedEventHandler(cb_SUBREGION_ADD_CUSTOMER_SelectionChanged);

            }
        }


        private void cb_MAINREGION_ADD_CUSTOMER_DropDownClosed(object sender, EventArgs e)
        {
            if (cb_MAINREGION_ADD_CUSTOMER.SelectedValue != null)
                Bind_cb_SUB_REGION_ADD_CUSTOMER((int)cb_MAINREGION_ADD_CUSTOMER.SelectedValue);
        }

        private void cb_SUBREGION_ADD_CUSTOMER_DropDownClosed(object sender, EventArgs e)
        {
            if (cb_SUBREGION_ADD_CUSTOMER.SelectedValue != null)
                Bind_cb_STREET_ADD_CUSTOMER((int)cb_SUBREGION_ADD_CUSTOMER.SelectedValue);
        }

        //----------------------------------ADDRESS MAINREGION--------------------------------------------------------
        private void btn_ADD_MAINREGION_ADDRESS_Click(object sender, RoutedEventArgs e)
        {
            if (tb_ADD_NEW_MAINREGION.Text.Trim() != "")
            {
                //bool xx = add.addNewMainREGION(tb_ADD_NEW_MAINREGION.Text.Trim());

                //if (xx)
                //{
                //    add.Bind_cb_REGION_WITH_TYPE(cb_ADDNEWMAINREGION_DELETE,3);
                //    MessageBox.Show("تم الإضافة بنجاح");
                //}
                //else
                //    MessageBox.Show("!خطأ .. هذة المنطقة موجودة من قبل");
                // MessageBox.Show("dd: "+xx);

                // add new main region
                PlacesViewModel vm = placesTab.DataContext as PlacesViewModel;
                bool saveResult = vm.SaveNewRegion();

                if (saveResult)
                    MessageBox.Show("تم الإضافة بنجاح");
                else
                    MessageBox.Show("!خطأ .. هذة المنطقة موجودة من قبل");


            }
            else
                MessageBox.Show("!خطأ .. من فضلك ادخل اسم المنطقة الرئيسية");
        }

        private void btn_ADDRESS_MAINREGION_DELETE_Click(object sender, RoutedEventArgs e)
        {
            if (cb_ADDNEWMAINREGION_DELETE.SelectedValue != null)
            {
                MessageBoxResult messageBoxResult = System.Windows.MessageBox.Show("هل انت متأكد من المسح؟", "تأكيد مسح منطقة رئيسية", System.Windows.MessageBoxButton.YesNo);
                if (messageBoxResult == MessageBoxResult.Yes)
                {
                    PlacesViewModel vm = placesTab.DataContext as PlacesViewModel;
                    Region selectedRegion = cb_ADDNEWMAINREGION_DELETE.SelectedItem as Region;

                    //if (add.checkDeleteREGIONWithParent((int)cb_ADDNEWMAINREGION_DELETE.SelectedValue) == 0)
                    if (vm.CanRemoveRegion(selectedRegion))
                    {
                        //if (add.deleteREGIONWithType((int)cb_ADDNEWMAINREGION_DELETE.SelectedValue, 3) > 0)
                        if (vm.RemoveRegion(selectedRegion))
                        {
                            //add.Bind_cb_REGION_WITH_TYPE(cb_ADDNEWMAINREGION_DELETE, 3);
                            MessageBox.Show("تم المسح بنجاح");
                        }
                        else
                            MessageBox.Show("!خطأ .. لا يمكن مسح هذه المنطقة");
                    }
                    else
                        MessageBox.Show("!خطأ .. لا يمكن مسح هذه المنطقة لأنها مرتبطة بمناطق اخرى او شوارع");
                }
            }
            else
                MessageBox.Show("!خطأ .. من فضلك اختر اسم المنطقة الرئيسية");

        }


        /* public DataSet GetDataFromDatabaseinDataSet()
         {
             DataSet ds = new DataSet();
             try
             {
                 // database Connection String
                 string connectionString = "Server = .; database = HKS; Integrated Security = true";
                 using (SqlConnection sqlCon = new System.Data.SqlClient.SqlConnection(connectionString))
                 {
                     using (SqlDataAdapter SqlDa = new SqlDataAdapter("usp_GetAllAddresses", sqlCon))
                     {
                         SqlDa.SelectCommand.CommandType = CommandType.StoredProcedure;
                         SqlDa.Fill(ds);
                     }
                 }
                 return ds;
             }
             catch (Exception)
             {
                 throw;
             }
         }*/

        //----------------------------------ADDRESS MAINREGION-------END-------------------------------------------------

        private void btn_ADD_NEWSUBREGION_Click(object sender, RoutedEventArgs e)
        {
            if (tb_ADDNEWSUBREGION_ADDRESS.Text.Trim() == "")
                MessageBox.Show("!خطأ .. من فضلك ادخل اسم المنطقة الفرعية");
            else if (cb_GETMAINREGION_SUBREGION_ADDRESS.SelectedValue == null)
                MessageBox.Show("!خطأ .. من فضلك اختر المنطقة الرئيسية");
            else
            {
                PlacesViewModel vm = placesTab.DataContext as PlacesViewModel;

                bool saveResult = vm.SaveNewSubRegion();


                //bool xx = add.addNewSubREGION(tb_ADDNEWSUBREGION_ADDRESS.Text.Trim(), (int)cb_GETMAINREGION_SUBREGION_ADDRESS.SelectedValue);

                if (saveResult)
                {
                    // add.Bind_cb_REGION_WITH_TYPE(cb_DELETE_SUBREGION_ADDRESS, 2);
                    // add.loadMainInSubRegionAddress(cb_GETMAINREGION_SUBREGION_ADDRESS);
                    MessageBox.Show("تم الإضافة بنجاح");
                }
                else
                    MessageBox.Show("!خطأ .. هذة المنطقة موجودة من قبل");

            }


        }

        private void btn_DELETE_SUBREGION_ADDRESS_Click(object sender, RoutedEventArgs e)
        {
            if (cb_DELETE_SUBREGION_ADDRESS.SelectedValue != null)
            {
                //   MessageBox.Show("ff " + (int)cb_ADDNEWMAINREGION_DELETE.SelectedValue);


                MessageBoxResult messageBoxResult = System.Windows.MessageBox.Show("هل انت متأكد من المسح؟", "تأكيد مسح منطقة فرعية", System.Windows.MessageBoxButton.YesNo);
                if (messageBoxResult == MessageBoxResult.Yes)
                {
                    PlacesViewModel vm = placesTab.DataContext as PlacesViewModel;
                    SubRegion selectedSubRegion = cb_DELETE_SUBREGION_ADDRESS.SelectedItem as SubRegion;

                    //if (add.checkDeleteREGIONWithParent((int)cb_DELETE_SUBREGION_ADDRESS.SelectedValue) == 0)
                    if (vm.CanRemoveSubRegion(selectedSubRegion))
                    {
                        //if (add.deleteREGIONWithType((int)cb_DELETE_SUBREGION_ADDRESS.SelectedValue, 2) > 0)
                        if (vm.RemoveSubRegion(selectedSubRegion))
                        {
                            //add.Bind_cb_REGION_WITH_PARENT(cb_DELETE_SUBREGION_ADDRESS, (int)cb_DELETE_MAINREGION_IN_SUBREGION_ADDRESS.SelectedValue);
                            //add.Bind_cb_REGION_WITH_TYPE(cb_DELETE_SUBREGION_ADDRESS, 2);
                            MessageBox.Show("تم المسح بنجاح");
                        }
                        else
                            MessageBox.Show("!خطأ .. لا يمكن مسح هذه المنطقة");
                    }
                    else
                        MessageBox.Show("!خطأ .. لا يمكن مسح هذه المنطقة لأنها مرتبطة بشوارع");
                }
            }
            else
                MessageBox.Show("!خطأ .. من فضلك اختر اسم المنطقة الفرعية");
        }

        private void cb_ADD_STREET_MAINREGION_ADDRESS_DropDownClosed(object sender, EventArgs e)
        {

            //if (cb_ADD_STREET_MAINREGION_ADDRESS.SelectedValue != null)
            //    add.Bind_cb_REGION_WITH_PARENT(cb_ADD_STREET_SUBREGION_ADDRESS, (int)cb_ADD_STREET_MAINREGION_ADDRESS.SelectedValue);

        }

        private void btn_ADD_STREET_ADDRESS_Click(object sender, RoutedEventArgs e)
        {
            if (tb_ADD_STREET_ADDRESS.Text.Trim() == "")
                MessageBox.Show("!خطأ .. من فضلك ادخل اسم الشارع");
            else if (cb_ADD_STREET_MAINREGION_ADDRESS.SelectedValue == null)
                MessageBox.Show("!خطأ .. من فضلك اختر المنطقة الرئيسية");
            else if (cb_ADD_STREET_SUBREGION_ADDRESS.SelectedValue == null)
                MessageBox.Show("!خطأ .. من فضلك اختر المنطقة الفرعية");
            else
            {
                // bool xx = add.addNewStreet(tb_ADD_STREET_ADDRESS.Text.Trim(), (int)cb_ADD_STREET_SUBREGION_ADDRESS.SelectedValue);

                PlacesViewModel vm = placesTab.DataContext as PlacesViewModel;
                bool saveResult = vm.SaveNewStreet();

                //bool saveResult = 
                if (saveResult)
                {
                    //add.Bind_cb_REGION_WITH_TYPE(cb_DELETE_STREET_ADDRESS, 1);
                    // add.loadMainInSubRegionAddress(cb_GETMAINREGION_SUBREGION_ADDRESS);
                    MessageBox.Show("تم الإضافة بنجاح");
                }
                else
                    MessageBox.Show("!خطأ .. هذة المنطقة موجودة من قبل");
                // MessageBox.Show("dd: "+xx);
            }
        }

        private void btn_DELETE_STREET_ADDRESS_Click(object sender, RoutedEventArgs e)
        {
            if (cb_DELETE_STREET_ADDRESS.SelectedValue != null)
            {
                //   MessageBox.Show("ff " + (int)cb_ADDNEWMAINREGION_DELETE.SelectedValue);


                MessageBoxResult messageBoxResult = System.Windows.MessageBox.Show("هل انت متأكد من المسح؟", "تأكيد مسح شارع", System.Windows.MessageBoxButton.YesNo);
                if (messageBoxResult == MessageBoxResult.Yes)
                {
                    PlacesViewModel vm = placesTab.DataContext as PlacesViewModel;

                    //if (add.deleteREGIONWithType((int)cb_DELETE_STREET_ADDRESS.SelectedValue, 1) > 0)
                    if (vm.RemoveStreet())
                    {
                        //add.Bind_cb_REGION_WITH_PARENT(cb_DELETE_STREET_ADDRESS, (int)cb_DELETE_SUBREGION_IN_STREET_ADDRESS.SelectedValue);

                        //add.Bind_cb_REGION_WITH_TYPE(cb_DELETE_STREET_ADDRESS, 1);
                        MessageBox.Show("تم المسح بنجاح");
                    }
                    else
                        MessageBox.Show("!خطأ .. لا يمكن مسح هذا الشارع");

                }
            }
            else
                MessageBox.Show("!خطأ .. من فضلك اختر اسم الشارع");
        }

        private void cb_DELETE_MAINREGION_IN_SUBREGION_ADDRESS_DropDownClosed(object sender, EventArgs e)
        {
            //if (cb_DELETE_MAINREGION_IN_SUBREGION_ADDRESS.SelectedValue != null)
            //    add.Bind_cb_REGION_WITH_PARENT(cb_DELETE_SUBREGION_ADDRESS, (int)cb_DELETE_MAINREGION_IN_SUBREGION_ADDRESS.SelectedValue);
        }

        private void cb_DELETE_MAINREGION_IN_STREET_ADDRESS_DropDownClosed(object sender, EventArgs e)
        {
            //if (cb_DELETE_MAINREGION_IN_STREET_ADDRESS.SelectedValue != null)
            //    add.Bind_cb_REGION_WITH_PARENT(cb_DELETE_SUBREGION_IN_STREET_ADDRESS, (int)cb_DELETE_MAINREGION_IN_STREET_ADDRESS.SelectedValue);
        }

        private void cb_DELETE_SUBREGION_IN_STREET_ADDRESS_DropDownClosed(object sender, EventArgs e)
        {
            //if (cb_DELETE_SUBREGION_IN_STREET_ADDRESS.SelectedValue != null)
            //    add.Bind_cb_REGION_WITH_PARENT(cb_DELETE_STREET_ADDRESS, (int)cb_DELETE_SUBREGION_IN_STREET_ADDRESS.SelectedValue);
        }


        //----------------------------ADDRESS END----------------------------------------
        //-----------------------------CUSTOMER---------------------------------------

        //public string validateAddNewCustomer()
        //{
        //    if (tb_NAME_ADD_CUSTOMER.Text.Trim() == "")
        //        return "!خطأ .. من فضلك ادخل الإسم بالكامل";
        //    else if (tb_MOBILE_ADD_CUSTOMER.Text.Trim() == "")
        //        return "!خطأ .. من فضلك ادخل الموبايل";
        //    else if (!checkNumber(tb_MOBILE_ADD_CUSTOMER.Text.Trim()))
        //        return "!خطأ .. من فضلك ادخل رقم موبايل صحيح";
        //    else if (tb_TELEPHONE_ADD_CUSTOMER.Text.Trim() == "")
        //        return "!خطأ .. من فضلك ادخل التليفون";
        //    else if (!checkNumber(tb_TELEPHONE_ADD_CUSTOMER.Text.Trim()))
        //        return "!خطأ .. من فضلك ادخل رقم تليفون صحيح";
        //    else if (tb_ADDRESS_ADD_CUSTOMER.Text.Trim() == "")
        //        return "!خطأ .. من فضلك ادخل العنوان بالكامل";
        //    else if (cb_MAINREGION_ADD_CUSTOMER.SelectedValue == null)
        //        return "!خطأ .. من فضلك اختر المنطقة الرئيسية";
        //    else if (cb_SUBREGION_ADD_CUSTOMER.SelectedValue == null)
        //        return "!خطأ .. من فضلك اختر المنطقة الفرعية";
        //    else if (cb_STREET_ADD_CUSTOMER.SelectedValue == null)
        //        return "!خطأ .. من فضلك اختر إسم الشارع";
        //    else if (!cb_HAVE_MAINTENANCE.IsChecked.Value && cb_MAINTENANCE_RATING.SelectedValue == null)
        //        return "!خطأ .. من فضلك اختر معدل الصيانة";
        //    return "true";
        //}

        private void btn_ADD_NEW_CUSTOMER_Click(object sender, RoutedEventArgs e)
        {
            tb_NAME_ADD_CUSTOMER.GetBindingExpression(TextBox.TextProperty).UpdateSource();
            tb_MOBILE_ADD_CUSTOMER.GetBindingExpression(TextBox.TextProperty).UpdateSource();
            tb_TELEPHONE_ADD_CUSTOMER.GetBindingExpression(TextBox.TextProperty).UpdateSource();
            tb_ADDRESS_ADD_CUSTOMER.GetBindingExpression(TextBox.TextProperty).UpdateSource();
            cb_STREET_ADD_CUSTOMER.GetBindingExpression(ComboBox.SelectedValueProperty).UpdateSource();

            if (HasError(tb_NAME_ADD_CUSTOMER) || HasError(tb_MOBILE_ADD_CUSTOMER) || HasError(tb_TELEPHONE_ADD_CUSTOMER) || HasError(tb_ADDRESS_ADD_CUSTOMER) || HasError(cb_STREET_ADD_CUSTOMER))
                return;

            //            string result = validateAddNewCustomer();
            //if (!result.Equals("true"))
            //    MessageBox.Show(result);
            //else
            //{

            AddCustomerViewModel vm = addCustomerTab.DataContext as AddCustomerViewModel;

            if (vm.SaveCustomer())
                MessageBox.Show("تم الإضافة بنجاح");
            else
                MessageBox.Show("خطأ غير متوقع");


            //}


            //MessageBox.Show("True Data");

        }

        private static bool checkNumber(string text)
        {
            Regex regex = new Regex("[^0-9.-]+"); //regex that matches disallowed text
            return !regex.IsMatch(text);
        }

        private void btn_SEARCH_CUSTOMER_Click(object sender, RoutedEventArgs e)
        {
            (customerSearchTab.DataContext as CustomerSearchViewModel).Search();

            //BindingOperations.ClearAllBindings(dataGrid1);

            //string customer_id_val = "0";
            //string mobile_val = "0";
            //string name_val = "0";

            //int customer_id_flag = 0;
            //int mobile_flag = 0;
            //int name_flag = 0;

            //customer_id_val = tb_CUSTOMER_ID_SEARCH_CUSTOMER.Text.Trim();
            //mobile_val = tb_MOBILE_SEARCH_CUSTOMER.Text.Trim();
            //name_val = tb_NAME_SEARCH_CUSTOMER.Text.Trim();

            //if (customer_id_val == "")
            //{
            //    customer_id_flag = 1;
            //    customer_id_val = "0";
            //}
            //if (mobile_val == "")
            //{
            //    mobile_flag = 1;
            //    mobile_val = "0";
            //}
            //if (name_val == "")
            //{
            //    name_flag = 1;
            //    name_val = "0";
            //}


            //cus.getCustomer(customer_id_val, customer_id_flag, name_val, name_flag, mobile_val, mobile_flag, dataGrid1);
        }
        //----------------------------------CUSTOMER END-----------------------------------------------
        //-----------------------------------INSTALLMENT----------------------------------------------

        private void btn_SHOW_INSTALLMENT_Click(object sender, RoutedEventArgs e)
        {
            //BindingOperations.ClearAllBindings(dg_SHOW_INSTALLMENT);

            //string customer_id_val = "0";
            //int customer_id_flag = 0;

            //customer_id_val = tb_CUSTOMER_ID_SHOW_INSTALLMENT.Text.Trim();

            //if (customer_id_val == "")
            //{
            //    customer_id_flag = 1;
            //    customer_id_val = "0";
            //}

            //ins.getInstallment(customer_id_val, customer_id_flag, dg_SHOW_INSTALLMENT);
        }

        private void tb_CUSTOMER_ID_ADD_NEW_INSTALLMENT_TextChanged(object sender, TextChangedEventArgs e)
        {
            //if (tb_CUSTOMER_ID_ADD_NEW_INSTALLMENT.Text.Trim() != "")
            //{
            //    tb_CUSTOMER_NAME_ADD_NEW_INSTALLMENT.Text = ins.getCustomerNameFromId(tb_CUSTOMER_ID_ADD_NEW_INSTALLMENT.Text.Trim());

            //}
        }

        private void btn_ADD_NEW_INSTALLMENT_Click(object sender, RoutedEventArgs e)
        {
            //string result = validateAddNewInstallment();
            //if (!result.Equals("true"))
            //    MessageBox.Show(result);
            //else if (ins.checkExistInstallment(tb_CUSTOMER_ID_ADD_NEW_INSTALLMENT.Text.Trim()) > 0)
            //{
            //    MessageBox.Show("!يوجد قسط لهذا العميل من قبل");
            //}
            //else if (Convert.ToInt16(tb_INITIAL_PAYMENT_ADD_NEW_INSTALLMENT.Text.Trim()) >= Convert.ToInt16(tb_TOTAL_ADD_NEW_INSTALLMENT.Text.Trim()))
            //{
            //    MessageBox.Show("!خطأ .. قيمة المقدم اكبر من الإجمالي");
            //}
            //else
            //{
            //    try
            //    {
            //        ins.addNewInstallment(tb_CUSTOMER_ID_ADD_NEW_INSTALLMENT.Text.Trim(),
            //            tb_CUSTOMER_NAME_ADD_NEW_INSTALLMENT.Text.Trim(),
            //            tb_TOTAL_ADD_NEW_INSTALLMENT.Text.Trim(),
            //            tb_INITIAL_PAYMENT_ADD_NEW_INSTALLMENT.Text.Trim()
            //            );
            //        MessageBox.Show("تم الإضافة بنجاح");
            //    }
            //    catch (Exception ee)
            //    {
            //        MessageBox.Show("خطأ غير متوقع");
            //    }
            //}
        }


        public string validateAddNewInstallment()
        {
            //if (tb_CUSTOMER_ID_ADD_NEW_INSTALLMENT.Text.Trim() == "")
            //    return "!خطأ .. كود العميل";
            //else if (!checkNumber(tb_CUSTOMER_ID_ADD_NEW_INSTALLMENT.Text.Trim()))
            //    return "!خطأ .. من فضلك ادخل كود عميل صحيح";
            //else if (tb_CUSTOMER_NAME_ADD_NEW_INSTALLMENT.Text.Trim() == "")
            //    return "!(N)خطأ .. من فضلك ادخل كود عميل صحيح";
            //if (tb_TOTAL_ADD_NEW_INSTALLMENT.Text.Trim() == "")
            //    return "!خطأ .. من فضلك ادخل الإجمالي";
            //else if (!checkNumber(tb_TOTAL_ADD_NEW_INSTALLMENT.Text.Trim()))
            //    return "!خطأ .. من فضلك ادخل إجمالي صحيح";
            //if (tb_INITIAL_PAYMENT_ADD_NEW_INSTALLMENT.Text.Trim() == "")
            //    return "!خطأ .. من فضلك ادخل المقدم";
            //else if (!checkNumber(tb_INITIAL_PAYMENT_ADD_NEW_INSTALLMENT.Text.Trim()))
            //    return "!خطأ .. من فضلك ادخل مقدم صحيح";
            //else
            return "true";
        }

        private void dg_SHOW_INSTALLMENT_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            IInputElement element = e.MouseDevice.DirectlyOver;
            if (element != null && element is FrameworkElement)
            {
                if (((FrameworkElement)element).Parent is DataGridCell)
                {
                    var grid = sender as DataGrid;
                    if (grid != null && grid.SelectedItems != null
        && grid.SelectedItems.Count == 1)
                    {
                        var rowView = grid.SelectedItem as DataRowView;
                        if (rowView != null)
                        {
                            DataRow row = rowView.Row;
                            //   MessageBox.Show((row.Field<Int32>("CUSTOMER_ID")).ToString());

                            //  new ins

                            var newMyWindow2 = new Window1((row.Field<Int32>("CUSTOMER_ID")).ToString(), (row.Field<String>("FULL_NAME")).ToString(), (row.Field<Int32>("INSTALLMENT_ID")).ToString(), (row.Field<Int32>("REMAINDER")).ToString());
                            //var newMyWindow2 = new Window1((row.Field<Int32>("INSTALLMENT_ID")).ToString());
                            newMyWindow2.Show();
                            //do something with the underlying data
                        }
                    }
                }
            }
        }

        private void cb_HAVE_MAINTENANCE_Checked(object sender, RoutedEventArgs e)
        {
            cb_MAINTENANCE_RATING.IsEnabled = false;
            lab_MAINTENANCE.IsEnabled = false;
        }

        private void cb_HAVE_MAINTENANCE_Unchecked(object sender, RoutedEventArgs e)
        {
            cb_MAINTENANCE_RATING.IsEnabled = true;
            lab_MAINTENANCE.IsEnabled = true;
        }

        public void Bind_cb_MAINTENANCE_RATING(ComboBox comboBoxName)
        {
            //  SqlConnection conn = new SqlConnection("Data Source=.\\SQLEXPRESS;AttachDbFilename=C:\\Users\\DELL\\Documents\\newd.mdf;Integrated Security=True;Connect Timeout=30;User Instance=True");
            string ConString = ConfigurationManager.ConnectionStrings["ConString"].ConnectionString;
            string CmdString = string.Empty;
            using (SqlConnection con = new SqlConnection(ConString))
            {
                CmdString = "SELECT MAINTENANCE_RATE FROM MAINTENANCE_RATING";
                SqlCommand cmd = new SqlCommand(CmdString, con);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds, "MAINTENANCE_RATING");
                comboBoxName.ItemsSource = ds.Tables[0].DefaultView;
                comboBoxName.DisplayMemberPath = ds.Tables[0].Columns["MAINTENANCE_RATE"].ToString();
                comboBoxName.SelectedValuePath = ds.Tables[0].Columns["MAINTENANCE_RATE"].ToString();

            }
        }

        private bool _isReportViewerLoaded;

        private void ReportViewer_Load(object sender, EventArgs e)
        {
            //   if (!_isReportViewerLoaded)
            //  {
            //    if (!_isReportViewerLoaded)
            //  {
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            newdDataSetRegion dataset = new newdDataSetRegion();

            dataset.BeginInit();

            reportDataSource1.Name = "DataSet_Region_Customers"; //Name of the report dataset in our .RDLC file
            reportDataSource1.Value = dataset.CUSTOMER;
            this.rv_Street_Customers.LocalReport.DataSources.Add(reportDataSource1);
            this.rv_Street_Customers.LocalReport.ReportEmbeddedResource = "MAIN.Report_Region_Customers.rdlc";

            //      _reportViewer.LocalReport.Load;

            dataset.EndInit();

            //fill data into adventureWorksDataSet
            //   newdDataSet_Region_Customers.SP_GET_REGION_CUSTOMERSDataTable salesOrderDetailTableAdapter = new newdDataSet_Region_Customers.SP_GET_REGION_CUSTOMERSDataTable();
            newdDataSetRegionTableAdapters.CUSTOMERTableAdapter salesOrderDetailTableAdapter = new newdDataSetRegionTableAdapters.CUSTOMERTableAdapter();
            salesOrderDetailTableAdapter.ClearBeforeFill = true;

            // salesOrderDetailTableAdapter.Clear();
            salesOrderDetailTableAdapter.Fill(dataset.CUSTOMER, 41);

            //  salesOrderDetailTableAdapter.;// = dataset.SP_GET_REGION_CUSTOMERS;

            rv_Street_Customers.RefreshReport();

            _isReportViewerLoaded = true;
            //    }
            //}
        }



        public void loadReport()
        {
            rv_Street_Customers.Clear();

            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            newdDataSetRegion dataset = new newdDataSetRegion();

            dataset.BeginInit();

            reportDataSource1.Name = "DataSet_Region_Customers"; //Name of the report dataset in our .RDLC file
            reportDataSource1.Value = dataset.CUSTOMER;
            this.rv_Street_Customers.LocalReport.DataSources.Add(reportDataSource1);
            this.rv_Street_Customers.LocalReport.ReportEmbeddedResource = "MAIN.Report_Region_Customers.rdlc";

            //      _reportViewer.LocalReport.Load;

            dataset.EndInit();

            //fill data into adventureWorksDataSet
            //   newdDataSet_Region_Customers.SP_GET_REGION_CUSTOMERSDataTable salesOrderDetailTableAdapter = new newdDataSet_Region_Customers.SP_GET_REGION_CUSTOMERSDataTable();
            newdDataSetRegionTableAdapters.CUSTOMERTableAdapter salesOrderDetailTableAdapter = new newdDataSetRegionTableAdapters.CUSTOMERTableAdapter();
            salesOrderDetailTableAdapter.ClearBeforeFill = true;

            // salesOrderDetailTableAdapter.Clear();
            salesOrderDetailTableAdapter.Fill(dataset.CUSTOMER, street_ID);

            //  salesOrderDetailTableAdapter.;// = dataset.SP_GET_REGION_CUSTOMERS;


            rv_Street_Customers.RefreshReport();
        }

        private void cb_DELETE_MAINREGION_IN_SUBREGION_ADDRESS_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void editCustomer_SearchBtn_Click(object sender, RoutedEventArgs e)
        {
            // force validation
            editCustomer_searchTxt.GetBindingExpression(TextBox.TextProperty).UpdateSource();

            // check for invalid id
            if (HasError(editCustomer_searchTxt))
                return;

            CustomerEditViewModel vm = editCustomerTab.DataContext as CustomerEditViewModel;
            if (!vm.Search())
                MessageBox.Show("خطأ .. برجاء التأكد من الكود");
        }

        private void editCustomer_BackBtn_Click(object sender, RoutedEventArgs e)
        {
            CustomerEditViewModel vm = editCustomerTab.DataContext as CustomerEditViewModel;
            vm.GoBack();
        }


        private void editCustomer_editBtn2_Click(object sender, RoutedEventArgs e)
        {
            // force form validation
            editCustomer_nameTxt.GetBindingExpression(TextBox.TextProperty).UpdateSource();
            editCustomer_mobileTxt.GetBindingExpression(TextBox.TextProperty).UpdateSource();
            editCustomer_telephoneTxt.GetBindingExpression(TextBox.TextProperty).UpdateSource();
            editCustomer_addressTxt.GetBindingExpression(TextBox.TextProperty).UpdateSource();
            editCustomer_streetCB.GetBindingExpression(ComboBox.SelectedValueProperty).UpdateSource();

            // check for invalid form inputs
            if (HasError(editCustomer_nameTxt) || HasError(editCustomer_mobileTxt) || HasError(editCustomer_telephoneTxt) || HasError(editCustomer_addressTxt) || HasError(editCustomer_streetCB))
                return;

            CustomerEditViewModel vm = editCustomerTab.DataContext as CustomerEditViewModel;
            if (vm.Edit())
                MessageBox.Show("تم التعديل بنجاح");
            else
                MessageBox.Show("خطأ!");
        }

        private bool HasError(FrameworkElement c)
        {
            return (bool)c.GetValue(Validation.HasErrorProperty);
        }


        // add installment button
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            addInstallment_CustomerCB.GetBindingExpression(ComboBox.SelectedValueProperty).UpdateSource();
            addInstallment_totalTxt.GetBindingExpression(TextBox.TextProperty).UpdateSource();
            addInstallment_ForepartTxt.GetBindingExpression(TextBox.TextProperty).UpdateSource();

            if (HasError(addInstallment_CustomerCB) || HasError(addInstallment_ForepartTxt) || HasError(addInstallment_totalTxt))
                return;

            InstallmentsAddViewModel vm = addInstallmentTab.DataContext as InstallmentsAddViewModel;
            if (vm.AddInstallment())
                MessageBox.Show("تم الإضافة بنجاح");
            else
                MessageBox.Show("خطأ .. برجاء التأكد من البيانات");


        }

        private void InstallmentDetailsBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Installment installment = (sender as Button).DataContext as Installment;
                InstallmentDetailsWindow window = new InstallmentDetailsWindow(installment.Id);
                window.ShowDialog();

                // refresh data grid
                InstallmentsSearchViewModel vm = installmentsSearchTab.DataContext as InstallmentsSearchViewModel;
                vm.UpdateInstallments();
            }
            catch (Exception) { }
        }

        private void addItemBtn_Click(object sender, RoutedEventArgs e)
        {
            addItemName.GetBindingExpression(TextBox.TextProperty).UpdateSource();
            addItemPrice.GetBindingExpression(TextBox.TextProperty).UpdateSource();
            addItemAmount.GetBindingExpression(TextBox.TextProperty).UpdateSource();

            if (HasError(addItemName) || HasError(addItemPrice) || HasError(addItemAmount))
                return;

            ItemsAddViewModel vm = addItemTab.DataContext as ItemsAddViewModel;

            string err;
            if (vm.AddItem(out err))
            {
                MessageBox.Show("تم الإضافة بنجاح");
            }
            else
            {
                MessageBox.Show(err);
            }

        }

        private void itemsSearchBtn_Click(object sender, RoutedEventArgs e)
        {
            ItemsSearchViewModel vm = searchItemsTab.DataContext as ItemsSearchViewModel;
            vm.Search();
        }



        private void btn_Regions_Reports_Click(object sender, RoutedEventArgs e)
        {
            //if (cb_Region_Street_Regions_Reports.SelectedValue != null)
            //{
            //    MessageBox.Show(cb_Region_Street_Regions_Reports.SelectedValue.ToString());
            //    street_ID = (int)cb_Region_Street_Regions_Reports.SelectedValue;
            //    loadReport();
            //    //rv_Street_Customers.Load += ReportViewer_Load;
            //}
            // return "!خطأ .. من فضلك اختر المنطقة الفرعية";

            //using(FilterCompanyDataContext db = new FilterCompanyDataContext())
            //{
            //var customers = db.Customers.ToList();

            //System.Windows.Forms.BindingSource source = new System.Windows.Forms.BindingSource();

            //ReportDataSource reportSource = new ReportDataSource(rv_Street_Customers.LocalReport.GetDataSourceNames()[0], source);
            //source.DataSource = customers;

            //rv_Street_Customers.LocalReport.ReportEmbeddedResource = "";


            //rv_Street_Customers.LocalReport.DataSources.Add(reportSource);

            //rv_Street_Customers.ProcessingMode = ProcessingMode.Local;
            //rv_Street_Customers.RefreshReport();


            //FilterCompanyDataContext db = new FilterCompanyDataContext();
            //try
            //{
            //    var qry = db.Customers.AsEnumerable(); // 
            //    rv_Street_Customers.ProcessingMode = ProcessingMode.Local;
            //    rv_Street_Customers.LocalReport.ReportEmbeddedResource = "MAIN.Reports.Customers.rdlc"; // .Reports if the report isin the Reports folder not in the root
            //    ReportDataSource dataSource = new ReportDataSource("DS_customers", qry);
            //    rv_Street_Customers.LocalReport.DataSources.Clear();
            //    rv_Street_Customers.LocalReport.DataSources.Add(dataSource);

            //    rv_Street_Customers.RefreshReport();
            //}
            //catch (Exception ex)
            //{
            //    throw new Exception(ex.Message);
            //}

            //}

            using (FilterCompanyDataContext db = new FilterCompanyDataContext())
            {
                var customers = db.Customers.Select(c => new CustomerReport()
                {
                    Id = c.Id,
                    Name = c.Name,
                    Mobile = c.Mobile,
                    Telephone = c.Telephone,
                    Address = c.Address,
                    MaintenanceInterval = c.MaintenanceInterval
                }).ToList();

                DataSet ds = ReportsHelpers.GetDataSet<CustomerReport>(customers, "Customers");
                ReportDataSource rds = new ReportDataSource("DataSet1", ds.Tables[0]);

                //System.Windows.Forms.BindingSource bindingSource = new System.Windows.Forms.BindingSource();
                //bindingSource.DataSource = rds;

                rv_Street_Customers.ProcessingMode = ProcessingMode.Local;
                rv_Street_Customers.LocalReport.ReportEmbeddedResource = "MAIN.Reports.Customers.rdlc";

                rv_Street_Customers.LocalReport.DataSources.Clear();
                rv_Street_Customers.LocalReport.DataSources.Add(rds);

                rv_Street_Customers.LocalReport.Refresh();
                rv_Street_Customers.RefreshReport();

            }

        }

        private void customersReportsBtn_Click(object sender, RoutedEventArgs e)
        {
            CustomerSearchViewModel vm = customerSearchTab.DataContext as CustomerSearchViewModel;

            var customers = vm.GetReportData();

            DataSet ds = ReportsHelpers.GetDataSet<CustomerReport>(customers, "Customers");
            ReportDataSource rds = new ReportDataSource("DataSet1", ds.Tables[0]);

            ReportsWindow window = new ReportsWindow("MAIN.Reports.Customers.rdlc", rds);
            window.ShowDialog();
        }

        private void itemEdit_searchBtn_Click(object sender, RoutedEventArgs e)
        {
            editItem_searchTxt.GetBindingExpression(TextBox.TextProperty).UpdateSource();
            if (HasError(editItem_searchTxt))
                return;

            ItemsEditViewModel vm = itemsEditTab.DataContext as ItemsEditViewModel;
            if (!vm.Search())
                MessageBox.Show("برجاء التأكد من الكود!");
        }

        private void editItems_editBtn_Click(object sender, RoutedEventArgs e)
        {
            editItem_nameTxt.GetBindingExpression(TextBox.TextProperty).UpdateSource();
            editItem_priceTxt.GetBindingExpression(TextBox.TextProperty).UpdateSource();
            editItem_amountTxt.GetBindingExpression(TextBox.TextProperty).UpdateSource();

            if (HasError(editItem_nameTxt) || HasError(editItem_priceTxt) || HasError(editItem_amountTxt))
                return;

            string err;
            ItemsEditViewModel vm = itemsEditTab.DataContext as ItemsEditViewModel;
            if (vm.Edit(out err))
                MessageBox.Show("تم التعديل بنجاح");
            else
                MessageBox.Show(err);
        }

        private void editItems_backBtn_Click(object sender, RoutedEventArgs e)
        {
            ItemsEditViewModel vm = itemsEditTab.DataContext as ItemsEditViewModel;
            vm.GoBack();
        }

        private void addMaintenanceInvoiceBtn_Click(object sender, RoutedEventArgs e)
        {
            addMaintenanceInvoice_CustomerCB.GetBindingExpression(ComboBox.SelectedValueProperty).UpdateSource();

            if (HasError(addMaintenanceInvoice_CustomerCB))
                return;

            MaintenanceAddInvoiceViewModel vm = addMaintenanceInvoiceTab.DataContext as MaintenanceAddInvoiceViewModel;

            string err;
            if (vm.AddInvoice(out err))
                MessageBox.Show("تم اللإضافة بنجاح");
            else
                MessageBox.Show(err);
        }

        private void addMaintenanceInvoiceUnitBtn_Click(object sender, RoutedEventArgs e)
        {
            addMaintenanceInvoiceUnit_ItemCB.GetBindingExpression(ComboBox.SelectedValueProperty).UpdateSource();
            addMaintenanceInvoiceUnit_nItemsTxt.GetBindingExpression(TextBox.TextProperty).UpdateSource();

            if (HasError(addMaintenanceInvoiceUnit_nItemsTxt) || HasError(addMaintenanceInvoiceUnit_ItemCB))
                return;

            MaintenanceAddInvoiceViewModel vm = addMaintenanceInvoiceTab.DataContext as MaintenanceAddInvoiceViewModel;
            string err;
            if (!vm.AddUnit(out err))
                MessageBox.Show(err);

        }

        private void addMaintenanceInvoice_removeUnit_Click(object sender, RoutedEventArgs e)
        {
            MaintenanceAddInvoiceViewModel vm = addMaintenanceInvoiceTab.DataContext as MaintenanceAddInvoiceViewModel;
            MaintenanceInvoiceUnit unit = (sender as Button).DataContext as MaintenanceInvoiceUnit;
            vm.RemoveUnit(unit);
        }

        private void editMaintenance_BackBtn_Click(object sender, RoutedEventArgs e)
        {
            MaintenanceEditInvoiceViewModel vm = maintenanceInvoiceEditTab.DataContext as MaintenanceEditInvoiceViewModel;
            vm.GoBack();
        }

        private void editMaintenance_searchBtn_Click(object sender, RoutedEventArgs e)
        {
            editMaintenance_searchTxt.GetBindingExpression(TextBox.TextProperty).UpdateSource();
            if (HasError(editMaintenance_searchTxt))
                return;

            MaintenanceEditInvoiceViewModel vm = maintenanceInvoiceEditTab.DataContext as MaintenanceEditInvoiceViewModel;
            if (!vm.Search())
                MessageBox.Show("برجاء التأكد من الكود");
        }

        private void editMaintenanceInvoice_removeUnit_Click(object sender, RoutedEventArgs e)
        {
            MaintenanceEditInvoiceViewModel vm = maintenanceInvoiceEditTab.DataContext as MaintenanceEditInvoiceViewModel;
            MaintenanceInvoiceUnit unit = (sender as Button).DataContext as MaintenanceInvoiceUnit;
            vm.RemoveUnit(unit);
        }

        private void editMaintenanceInvoiceBtn_Click(object sender, RoutedEventArgs e)
        {
            editMaintenanceInvoice_CustomerCB.GetBindingExpression(ComboBox.SelectedValueProperty).UpdateSource();
            if (HasError(editMaintenanceInvoice_CustomerCB))
                return;

            MaintenanceEditInvoiceViewModel vm = maintenanceInvoiceEditTab.DataContext as MaintenanceEditInvoiceViewModel;
            string err;
            if (vm.SaveChanges(out err))
                MessageBox.Show("تم التعديل بنجاح");
            else
                MessageBox.Show(err);
        }

        private void editMaintenanceInvoiceUnitBtn_Click(object sender, RoutedEventArgs e)
        {
            editMaintenanceInvoiceUnit_ItemCB.GetBindingExpression(ComboBox.SelectedValueProperty).UpdateSource();
            editMaintenanceInvoiceUnit_nItemsTxt.GetBindingExpression(TextBox.TextProperty).UpdateSource();

            if (HasError(editMaintenanceInvoiceUnit_nItemsTxt) || HasError(editMaintenanceInvoiceUnit_ItemCB))
                return;

            MaintenanceEditInvoiceViewModel vm = maintenanceInvoiceEditTab.DataContext as MaintenanceEditInvoiceViewModel;
            string err;
            if (!vm.AddUnit(out err))
                MessageBox.Show(err);
        }

        private void deleteMaintenanceInvoiceBtn_Click(object sender, RoutedEventArgs e)
        {
            MaintenanceEditInvoiceViewModel vm = maintenanceInvoiceEditTab.DataContext as MaintenanceEditInvoiceViewModel;
            string err;
            if (!vm.Delete(out err))
                MessageBox.Show(err);
            else
                MessageBox.Show("تم الحذف بنجاح");
        }

        private void incomingsReportBtn_Click(object sender, RoutedEventArgs e)
        {
            IncomingsViewModel vm = incomingsTab.DataContext as IncomingsViewModel;
            DataSet ds = ReportsHelpers.GetDataSet<Income>(vm.Results.ToList(), "Customers");
            ReportDataSource rds = new ReportDataSource("DataSet1", ds.Tables[0]);

            ReportsWindow window = new ReportsWindow("MAIN.Reports.IncomingsReport.rdlc", rds);

            window.ShowDialog();
        }

        private void internalOutgoingAddBtn_Click(object sender, RoutedEventArgs e)
        {
            internalOutgoingValueTxt.GetBindingExpression(TextBox.TextProperty).UpdateSource();
            if (HasError(internalOutgoingValueTxt))
                return;

            InternalOutgoingsAddViewModel vm = internaloutgoingAddTab.DataContext as InternalOutgoingsAddViewModel;
            string err;
            if (vm.Save(out err))
                MessageBox.Show("تم الحفظ بنجاح");
            else
                MessageBox.Show(err);
        }

        private void internalOutgoingEdit_backBtn_Click(object sender, RoutedEventArgs e)
        {
            InternalOutgoingsEditViewModel vm = internalOutgoingEditTab.DataContext as InternalOutgoingsEditViewModel;
            vm.GoBack();
        }

        private void internaloutgoingEdit_searchBtn_Click(object sender, RoutedEventArgs e)
        {
            InternalOutgoingsEditViewModel vm = internalOutgoingEditTab.DataContext as InternalOutgoingsEditViewModel;
            if (!vm.Search())
                MessageBox.Show("برجاء التأكد من الكود");
        }

        private void internalOutgoingEdit_delBtn_Click(object sender, RoutedEventArgs e)
        {
            InternalOutgoingsEditViewModel vm = internalOutgoingEditTab.DataContext as InternalOutgoingsEditViewModel;
            if (vm.Delete())
                MessageBox.Show("تم الحذف بنجاح");
            else
                MessageBox.Show("حدث خطأ غير متوقع");
        }

        private void internalOutgoingEdit_editBtn_Click(object sender, RoutedEventArgs e)
        {
            InternalOutgoingsEditViewModel vm = internalOutgoingEditTab.DataContext as InternalOutgoingsEditViewModel;
            string err;
            if (vm.SaveChanges(out err))
                MessageBox.Show("تم التعديل بنجاح");
            else
                MessageBox.Show(err);
        }

        private void externalOutgoingsAddBtn_Click(object sender, RoutedEventArgs e)
        {
            externalOutgoingAdd_priceTxt.GetBindingExpression(TextBox.TextProperty).UpdateSource();
            externalOutgoingAdd_amountTxt.GetBindingExpression(TextBox.TextProperty).UpdateSource();

            if (HasError(externalOutgoingAdd_amountTxt) || HasError(externalOutgoingAdd_priceTxt) || HasError(externaloutgoingAdd_ItemCB))
                return;

            ExternalOutgoingsAddViewModel vm = externalOutgoingsAddTab.DataContext as ExternalOutgoingsAddViewModel;
            string err;
            if (vm.Save(out err))
                MessageBox.Show("تم الحفظ بنجاح");
            else
                MessageBox.Show(err);
        }

        private void externalOutgoingsEdit_searchBtn_Click(object sender, RoutedEventArgs e)
        {
            externalOutgoingsEdit_codeTxt.GetBindingExpression(TextBox.TextProperty).UpdateSource();

            if (HasError(externalOutgoingsEdit_codeTxt))
                return;

            ExternalOutgoingsEditViewModel vm = externalOutgoingsEditTab.DataContext as ExternalOutgoingsEditViewModel;
            if (!vm.Search())
                MessageBox.Show("برجاء التأكد من الكود");
        }

        private void externalOutgoingsEdit_backBtn_Click(object sender, RoutedEventArgs e)
        {
            ExternalOutgoingsEditViewModel vm = externalOutgoingsEditTab.DataContext as ExternalOutgoingsEditViewModel;
            vm.GoBack();
        }

        private void externalOutgoingsDeleteBtn_Click(object sender, RoutedEventArgs e)
        {
            ExternalOutgoingsEditViewModel vm = externalOutgoingsEditTab.DataContext as ExternalOutgoingsEditViewModel;
            if (vm.Delete())
                MessageBox.Show("تم الحذف بنجاح");
            else
                MessageBox.Show("حدث خطأ غير متوقع");
        }

        private void externalOutgoingsEditBtn_Click(object sender, RoutedEventArgs e)
        {
            ExternalOutgoingsEditViewModel vm = externalOutgoingsEditTab.DataContext as ExternalOutgoingsEditViewModel;
            string err;
            if (vm.Save(out err))
                MessageBox.Show("تم الحفظ بنجاح");
            else
                MessageBox.Show(err);
        }

        private void maintenanceInvoiceReportBtn_Click(object sender, RoutedEventArgs e)
        {
            MaintenanceInvoice invoice = (sender as Button).DataContext as MaintenanceInvoice;

            if (invoice == null)
                return;
            //IncomingsViewModel vm = incomingsTab.DataContext as IncomingsViewModel;
            using (FilterCompanyDataContext db = new FilterCompanyDataContext())
            {
                var units = db.MaintenanceInvoiceUnits.Where(u => u.MaintenanceInvoiceId == invoice.Id).Include(u => u.Item)
                    .Select(u => new InvoiceUnitRep()
                    {
                        Amount = u.Amount,
                        Item = u.Item.Name,
                        NumberOfItems = u.NumberOfItems
                    }).ToList();


                DataSet ds = ReportsHelpers.GetDataSet<InvoiceUnitRep>(units, "ReportName");
                ReportDataSource rds = new ReportDataSource("DataSet2", ds.Tables[0]);

                List<ReportParameter> parameters = new List<ReportParameter>();
                ReportParameter parameter = new ReportParameter();
                parameter.Name = "CustomerName";
                parameter.Values.Add(invoice.Customer.Name);
                parameters.Add(parameter);

                parameters.Add(ReportsHelpers.CreateReportParameter("Address", invoice.Address));
                parameters.Add(ReportsHelpers.CreateReportParameter("Date", invoice.InvoiceDate.ToShortDateString()));
                parameters.Add(ReportsHelpers.CreateReportParameter("Total", invoice.TotalAmount.ToString()));
                parameters.Add(ReportsHelpers.CreateReportParameter("Notes", invoice.Notes));

                ReportsWindow window = new ReportsWindow("MAIN.Reports.MaintenanceInvoice.rdlc", rds, parameters);

                window.ShowDialog();
            }

        }


        private void placesTab_GotFocus(object sender, RoutedEventArgs e)
        {
            //if (e.OriginalSource == placesTab)
            //    (placesTab.DataContext as PlacesViewModel).LoadRegions();
        }

        private void SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.OriginalSource == mainTabControl)
            {
                if (mainTabControl.SelectedItem == placesTab)
                {
                    (placesTab.DataContext as PlacesViewModel).LoadRegions();
                }
                else if(mainTabControl.SelectedItem == customersTab)
                {
                    (addCustomerTab.DataContext as AddCustomerViewModel).LoadRegions();
                    (customerSearchTab.DataContext as CustomerSearchViewModel).Clear();
                    (editCustomerTab.DataContext as CustomerEditViewModel).LoadRegions();
                }
                else if(mainTabControl.SelectedItem == installmentsViewTab)
                {
                    (addInstallmentTab.DataContext as InstallmentsAddViewModel).LoadCustomers();
                    (installmentsSearchTab.DataContext as InstallmentsSearchViewModel).LoadCustomers();
                }
            }
        }

    }

}
