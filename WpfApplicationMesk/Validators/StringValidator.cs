﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace MAIN.Validators
{
    public class StringValidator : ValidationRule
    {
        public override ValidationResult Validate(object value, System.Globalization.CultureInfo cultureInfo)
        {
            string s = value.ToString();
            return new ValidationResult((!string.IsNullOrEmpty(s) && s.Length > 3 && s.Length < 6), "string must be between 3 and 6 chars!!");
        }
    }
}
