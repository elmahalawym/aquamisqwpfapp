﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace MAIN.Validators
{
    public class RequiredFieldValidator : ValidationRule
    {
        public string ErrorMessage { get; set; }

        public override ValidationResult Validate(object value, System.Globalization.CultureInfo cultureInfo)
        {
            string s = (value == null) ? "" : value.ToString();
            return new ValidationResult(!string.IsNullOrEmpty(s), ErrorMessage);
        }
    }
}
