﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace MAIN.Validators
{
    public class NumberValidator : ValidationRule
    {
        public string ErrorMessage { get; set; }
        public override ValidationResult Validate(object value, System.Globalization.CultureInfo cultureInfo)
        {
            int i;
            return new ValidationResult(value != null && int.TryParse(value.ToString(), out i), ErrorMessage);
        }
    }
}
