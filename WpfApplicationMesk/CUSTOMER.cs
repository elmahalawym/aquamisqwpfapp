﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System.Windows.Controls;

namespace MAIN
{
    class CUSTOMER
    {
        public void addNewCustomer(string name, string mobile, string tel, string address, int street_id, int maintenance_rating )
        {
            //  SqlConnection conn = new SqlConnection("Data Source=.\\SQLEXPRESS;AttachDbFilename=C:\\Users\\DELL\\Documents\\newd.mdf;Integrated Security=True;Connect Timeout=30;User Instance=True");
            string ConString = ConfigurationManager.ConnectionStrings["ConString"].ConnectionString;
            string CmdString = string.Empty;
            using (SqlConnection con = new SqlConnection(ConString))
            {
                SqlCommand cmd = con.CreateCommand();
                cmd.CommandType = CommandType.StoredProcedure;

                con.Open();

                cmd.CommandText = "SP_ADD_NEW_CUSTOMER";
                cmd.Parameters.Add("@FullName", SqlDbType.NVarChar, 0).Value = name;
                cmd.Parameters.Add("@Telephone", SqlDbType.NVarChar, 0).Value = tel ;
                cmd.Parameters.Add("@Mobile", SqlDbType.NVarChar, 0).Value = mobile;
                cmd.Parameters.Add("@Address", SqlDbType.NVarChar, 0).Value = address;
                cmd.Parameters.Add("@StreetID", SqlDbType.Int, 0).Value = street_id;
                cmd.Parameters.Add("@MaintenanceRating", SqlDbType.Int, 0).Value = maintenance_rating;

                cmd.ExecuteNonQuery();
            }
        }


        public void getCustomer(string customer_id_val, int customer_id_flag, string name_val, int name_flag, string mobile_val, int mobile_flag, DataGrid dg)
        {
            string ConString = ConfigurationManager.ConnectionStrings["ConString"].ConnectionString;
            string CmdString = string.Empty;
            using (SqlConnection con = new SqlConnection(ConString))
            {
                SqlCommand cmd = con.CreateCommand();
                cmd.CommandType = CommandType.StoredProcedure;

                con.Open();

                cmd.CommandText = "SP_GET_CUSTOMER";
                cmd.Parameters.Add("@CustomerID", SqlDbType.Int, 0).Value = customer_id_val;
                cmd.Parameters.Add("@CustomerIDFlag", SqlDbType.Int, 0).Value = customer_id_flag;
                cmd.Parameters.Add("@FullName", SqlDbType.NVarChar, 0).Value = name_val;
                cmd.Parameters.Add("@FullNameFlag", SqlDbType.Int, 0).Value = name_flag;
                cmd.Parameters.Add("@Mobile", SqlDbType.NVarChar, 0).Value = mobile_val;
                cmd.Parameters.Add("@MobileFlag", SqlDbType.Int, 0).Value = mobile_flag;
                DataSet ds = new DataSet();


                using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                {
                    da.Fill(ds);

                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        // Binding the Grid with the Itemsource property
                        dg.ItemsSource = ds.Tables[0].DefaultView;
                    }
                
                  //  comboBoxName.ItemsSource = ds.Tables[0].DefaultView;
                  //  comboBoxName.DisplayMemberPath = ds.Tables[0].Columns["NAME"].ToString();
                  //  comboBoxName.SelectedValuePath = ds.Tables[0].Columns["ID"].ToString();
                }

            }
        }
    }
}
