﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAIN.Models
{
    public class ExternalOutgoing
    {
        [Key]
        public int Id { get; set; }

        [ForeignKey("Item")]
        public int ItemId { get; set; }

        public virtual Item Item { get; set; }

        public decimal Price { get; set; }

        public int Amount { get; set; }

        public DateTime OutgoingDate { get; set; }

        public string Notes { get; set; }
    }
}
