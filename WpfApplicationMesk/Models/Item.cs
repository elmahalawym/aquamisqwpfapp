﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAIN.Models
{
    public class Item
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        public int Price { get; set; }

        public int Amount { get; set; } // الكمية الموجودة في المخزن

        public string Notes { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }
}
