﻿using MAIN.Migrations;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAIN.Models
{
    public class FilterCompanyDataContext : DbContext
    {
        static FilterCompanyDataContext()
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<FilterCompanyDataContext, Configuration>());
        }

        public FilterCompanyDataContext() : base("MyDB") { }


        public DbSet<Customer> Customers { get; set; }
        public DbSet<Region> Regions { get; set; }
        public DbSet<SubRegion> SubRegions { get; set; }
        public DbSet<Street> Streets { get; set; }
        public DbSet<Installment> Installments { get; set; }
        public DbSet<InstallmentUnit> InstallmentUnits { get; set; }
        public DbSet<Item> Items { get; set; }
        public DbSet<MaintenanceInvoice> MaintenanceInvoices { get; set; }
        public DbSet<MaintenanceInvoiceUnit> MaintenanceInvoiceUnits { get; set; }
        public DbSet<InternalOutgoing> InternalOutgoings { get; set; }
        public DbSet<ExternalOutgoing> ExternalOutgoings { get; set; }

    }
}
