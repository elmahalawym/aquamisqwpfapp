﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAIN.Models
{
    public class InvoiceUnitRep
    {
        public string Item { get; set; }
        public int NumberOfItems { get; set; }
        public int Amount { get; set; }
    }
}
