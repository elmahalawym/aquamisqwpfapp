﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAIN.Models
{
    public class MaintenanceInvoiceUnit : ViewModels.ViewModelsBase
    {
        [Key]
        public int Id { get; set; }

        [ForeignKey("MaintenanceInvoice")]
        public int MaintenanceInvoiceId { get; set; }

        public virtual MaintenanceInvoice MaintenanceInvoice { get; set; }

        [ForeignKey("Item")]
        public int ItemId { get; set; }

        public virtual Item Item { get; set; }

        private int numberOfItems;
        public int NumberOfItems {
            get { return numberOfItems; }
            set
            {
                numberOfItems = value;
                NotifyPropertyChanged("NumberOfItems");
            }
        }

        private int amount;
        public int Amount
        {
            get { return amount; }
            set
            {
                amount = value;
                NotifyPropertyChanged("Amount");
            }
        }

        public override string ToString()
        {
            return Item.Name;
        }
    }
}
