﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAIN.Models
{
    public class InternalOutgoing
    {
        [Key]
        public int Id { get; set; }

        public DateTime OutgoingDate { get; set; }

        public int Amount { get; set; }

        public string Notes { get; set; }
    }
}
