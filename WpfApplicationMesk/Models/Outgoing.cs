﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAIN.Models
{
    public class Outgoing // Generalization for ExternalOutgoings and InternalOutgoings
    {
        public int Id { get; set; }
        public DateTime OutgoingDate { get; set; }
        public string OutgoingType { get; set; }
        public int Amount { get; set; }
        public string Notes { get; set; }
    }
}
