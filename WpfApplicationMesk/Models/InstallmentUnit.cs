﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAIN.Models
{
    public class InstallmentUnit
    {
        [Key]
        public int Id { get; set; }

        [ForeignKey("Installment")]
        public int InstallmentId { get; set; }

        public virtual Installment Installment { get; set; }

        public int Amount { get; set; }

        public DateTime Date { get; set; }
    }
}
