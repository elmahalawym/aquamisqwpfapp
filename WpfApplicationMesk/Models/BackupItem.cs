﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MAIN.Models
{
    public class BackupItem
    {
        public string FullName { get; set; }

        public DateTime CreateDateTime { get; set; }
    }
}
