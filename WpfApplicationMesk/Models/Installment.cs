﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Documents;

namespace MAIN.Models
{
    public class Installment
    {

        [Key]
        public int Id { get; set; }

        [ForeignKey("Customer")]
        public int CustomerId { get; set; }

        public virtual Customer Customer { get; set; }

        public int Total { get; set; }

        public int PaidAmount { get; set; }

        public DateTime InstallmentDate { get; set; }

        public DateTime? NextDate { get; set; }


        public virtual ICollection<InstallmentUnit> InstallmentUnits { get; set; }
    }
}
