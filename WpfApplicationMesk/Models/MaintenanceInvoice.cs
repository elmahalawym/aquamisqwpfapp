﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAIN.Models
{
    public class MaintenanceInvoice
    {
        [Key]
        public int Id { get; set; }

        [ForeignKey("Customer")]
        public int CustomerId { get; set; }

        public virtual Customer Customer { get; set; }

        public DateTime InvoiceDate { get; set; }

        public string Address { get; set; }


        public int TotalAmount { get; set; }

        public string Notes { get; set; }

        public virtual ICollection<MaintenanceInvoiceUnit> MaintenanceInvoiceUnits { get; set; }
    }
}
