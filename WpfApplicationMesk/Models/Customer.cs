﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace MAIN.Models
{
    public class Customer
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Mobile { get; set; }
        public string Telephone { get; set; }

        public string Address { get; set; }

        [ForeignKey("Street")]
        public int StreetId { get; set; }

        public virtual Street Street { get; set; }

        public int MaintenanceInterval { get; set; }

        public DateTime? NextPaymentDate { get; set; }


        public virtual IEnumerable<MaintenanceInvoice> MaintenanceInvoices { get; set; }
    }
}
