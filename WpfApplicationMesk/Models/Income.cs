﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAIN.Models
{
    public class Income // generalization for installment units and maintenance invoices
    {
        public string CustomerName { get; set; }

        public DateTime IncomeDate { get; set; }

        public string Type { get; set; }

        public int Value { get; set; }
    }
}
