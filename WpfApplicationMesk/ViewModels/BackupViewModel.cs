﻿/*
 * backup file name format: {year}_{month}_{day}_{hour}_{minute}_{second}_DbBackup.sdf
*/

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.IO;
using MAIN.Models;

namespace MAIN.ViewModels
{
    class BackupViewModel : ViewModelsBase
    {
        private const string BACKUPS_PATH = @"C:/Backups/";

        public BackupViewModel()
        {
            // initialization
            Backups = new ObservableCollection<BackupItem>();

            // load backups
            LoadBackups();
        }

        public ObservableCollection<BackupItem> Backups { get; set; }

        public void LoadBackups()
        {
            try
            {
                // create directory if not exists
                if (!Directory.Exists(BACKUPS_PATH))
                    Directory.CreateDirectory(BACKUPS_PATH);

                // get backup files
                DirectoryInfo dInfo = new DirectoryInfo(BACKUPS_PATH);
                var backupFiles = dInfo.GetFiles("*.sdf");

                // load data
                foreach (var file in backupFiles)
                {
                    string[] nameParts = file.Name.Split('_');
                    Backups.Add(new BackupItem
                        {
                            FullName = file.FullName,
                            //CreateDateTime = new DateTime()
                        });
                    
                }
            }
            catch (Exception) { }

            throw new NotImplementedException();
        }


    }
}
