﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using MAIN.Models;
using System.Data.Entity;

namespace MAIN.ViewModels
{
    public class MaintenanceInvoiceSearchViewModel : ViewModelsBase
    {
        public MaintenanceInvoiceSearchViewModel()
        {
            Customers = new ObservableCollection<Customer>();
            Invoices = new ObservableCollection<MaintenanceInvoice>();
            LoadCustomers();
        }

        public ObservableCollection<Customer> Customers { get; set; }
        public ObservableCollection<MaintenanceInvoice> Invoices { get; set; }

        private int? customerId;
        public int? CustomerId
        {
            get { return customerId; }
            set
            {
                customerId = value;
                LoadInvoices();
                NotifyPropertyChanged("CustomerId");
            }
        }



        private void LoadCustomers()
        {
            Customers.Clear();
            using(FilterCompanyDataContext db = new FilterCompanyDataContext())
            {
                foreach (var customer in db.Customers)
                    Customers.Add(customer);
            }
        }

        private void LoadInvoices()
        {
            Invoices.Clear();
            if (CustomerId != null)
            {
                using (FilterCompanyDataContext db = new FilterCompanyDataContext())
                {
                    var invoicesFromDb = db.MaintenanceInvoices.Where(i => i.CustomerId == (int)CustomerId).Include(i => i.Customer).OrderByDescending(i => i.InvoiceDate).ToList();
                    foreach (var invoice in invoicesFromDb)
                        Invoices.Add(invoice);
                }
            }

        }
    }
}
