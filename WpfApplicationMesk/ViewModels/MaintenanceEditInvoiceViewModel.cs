﻿using MAIN.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace MAIN.ViewModels
{
    public class MaintenanceEditInvoiceViewModel : ViewModelsBase
    {
        private Item _item;
        public MaintenanceEditInvoiceViewModel()
        {
            Customers = new ObservableCollection<object>();
            InvoiceUnits = new ObservableCollection<MaintenanceInvoiceUnit>();
            Items = new ObservableCollection<Item>();

            LoadCustomers();
            LoadItems();
        }

        public ObservableCollection<object> Customers { get; set; }
        public ObservableCollection<MaintenanceInvoiceUnit> InvoiceUnits { get; set; }

        public ObservableCollection<Item> Items { get; set; }

        private string invoiceId;
        public string InvoiceId
        {
            get { return invoiceId; }
            set
            {
                invoiceId = value;
                NotifyPropertyChanged("InvoiceId");
            }
        }

        private int? customerId;
        public int? CustomerId
        {
            get { return customerId; }
            set
            {
                customerId = value;
                NotifyPropertyChanged("CustomerId");
            }
        }

        private string address;
        public string Address
        {
            get { return address; }
            set
            {
                address = value;
                NotifyPropertyChanged("Address");
            }
        }

        private DateTime invoiceDate = DateTime.Today; // default value
        public DateTime InvoiceDate
        {
            get { return invoiceDate; }
            set
            {
                invoiceDate = value;
                NotifyPropertyChanged("InvoiceDate");
            }
        }

        private string notes;
        public string Notes
        {
            get { return notes; }
            set
            {
                notes = value;
                NotifyPropertyChanged("Notes");
            }
        }

        private int total;
        public int Total
        {
            get { return total; }
            private set
            {
                total = value;
                NotifyPropertyChanged("Total");
            }
        }

        private int? newUnitItemId;
        public int? NewUnitItemId
        {
            get { return newUnitItemId; }
            set
            {
                newUnitItemId = value;

                if (value != null)
                    using (FilterCompanyDataContext db = new FilterCompanyDataContext())
                        _item = db.Items.Find((int)value);

                if(_item != null)
                    NewUnitTotal = _item.Price * NewUnitNumberOfItems;
                else
                    NewUnitTotal = 0;

                NotifyPropertyChanged("NewUnitItemId");
            }
        }


        private int newUnitNumberOfItems;
        public int NewUnitNumberOfItems
        {
            get { return newUnitNumberOfItems; }
            set
            {
                newUnitNumberOfItems = value;

                if (_item != null)
                    NewUnitTotal = _item.Price * value;
                else
                    NewUnitTotal = 0;

                NotifyPropertyChanged("NewUnitNumberOfItems");
            }
        }

        private int newUnitTotal;
        public int NewUnitTotal
        {
            get { return newUnitTotal; }
            private set
            {
                newUnitTotal = value;
                NotifyPropertyChanged("NewUnitTotal");
            }
        }

        private bool mode;
        public bool Mode
        {
            get { return mode; }
            set
            {
                mode = value;
                NotifyPropertyChanged("Mode");
            }
        }

        public bool Search()
        {
            int id;
            if (int.TryParse(InvoiceId, out id))
            {
                // search for invoice
                using(FilterCompanyDataContext db = new FilterCompanyDataContext())
                {
                    MaintenanceInvoice invoice = db.MaintenanceInvoices.Find(id);
                    if (invoice == null) // not found
                        return false;
                    
                    // change mode, load invoice details
                    Mode = true;
                    CustomerId = invoice.CustomerId;
                    Address = invoice.Address;
                    InvoiceDate = invoice.InvoiceDate;
                    Notes = invoice.Notes;
                    Total = invoice.TotalAmount;

                    InvoiceUnits.Clear();
                    foreach (var unit in db.MaintenanceInvoiceUnits.Where(i => i.MaintenanceInvoiceId == id).Include(i => i.Item).ToList())
                        InvoiceUnits.Add(new MaintenanceInvoiceUnit()
                        {
                            Amount = unit.Amount,
                            ItemId = unit.ItemId,
                            MaintenanceInvoiceId = unit.MaintenanceInvoiceId,
                            NumberOfItems = unit.NumberOfItems,
                            Item = unit.Item
                        });

                    // succuess
                    return true;
                }
            }
            else
                return false;
        }


        public bool AddUnit(out string err)
        {
            err = "";
            if (NewUnitNumberOfItems <= 0)
            {
                err = "العدد لا يمكن أن يكون صفر أو أقل!";
                return false;
            }

            // only used when adding units for the first time
            //if(_item.Amount < (NewUnitNumberOfItems + InvoiceUnits.Where(i => i.ItemId == newUnitItemId).Sum(i => i.NumberOfItems)))
            //{
            //    err = "لا توجد كمية في المخزن!";
            //    return false;
            //}

            if (InvoiceUnits.Where(i => i.Item.Name.Trim() == _item.Name.Trim()).Count() == 0)
            {
                InvoiceUnits.Add(new MaintenanceInvoiceUnit()
                {
                    NumberOfItems = NewUnitNumberOfItems,
                    Amount = NewUnitTotal,
                    ItemId = (int)NewUnitItemId,
                    Item = _item
                });
            }
            else
            {
                // if item already exists
                var invoice = InvoiceUnits.Where(i => i.Item.Name.Trim() == _item.Name.Trim()).First();
                invoice.NumberOfItems += NewUnitNumberOfItems;
                invoice.Amount += NewUnitTotal;
            }

            Total = InvoiceUnits.Sum(u => u.Amount);

            NewUnitItemId = null;
            NewUnitNumberOfItems = 0;
            NewUnitTotal = 0;

            return true;
        }

        public void RemoveUnit(MaintenanceInvoiceUnit unit)
        {
            // remove unit
            InvoiceUnits.Remove(unit);

            // recalculate total
            Total = InvoiceUnits.Sum(i => i.Amount);
        }

        public bool SaveChanges(out string err)
        {
            err = "";
            if(InvoiceUnits.Count == 0)
            {
                err = "يجب أن تحتوي الفاتورة علي وحدات";
                return false;
            }


            try
            {
                using(FilterCompanyDataContext db = new FilterCompanyDataContext())
                {
                    // get selected invoice
                    int id = int.Parse(InvoiceId);
                    var invoice = db.MaintenanceInvoices.Find(id);

                    // edit values
                    invoice.CustomerId = (int)CustomerId;
                    invoice.Address = Address;
                    invoice.InvoiceDate = InvoiceDate;
                    invoice.Notes = Notes;
                    invoice.TotalAmount = Total;
                    
                    //>>>>>>>>>>> remove and readd invoice units
                    var curUnits = db.MaintenanceInvoiceUnits.Where(u => u.MaintenanceInvoiceId == id).ToList();

                    // add number of items in old units to store
                    foreach (var unit in curUnits)
                        unit.Item.Amount += unit.NumberOfItems;

                    // save changes
                    db.SaveChanges();

                    // delete old units
                    db.MaintenanceInvoiceUnits.RemoveRange(curUnits);

                    // add new units
                    foreach(var unit in InvoiceUnits)
                    {
                        unit.MaintenanceInvoiceId = invoice.Id;
                        unit.ItemId = unit.Item.Id;
                        unit.Item = null;
                        db.MaintenanceInvoiceUnits.Add(unit);

                        // remove number of items from store
                        Item item = db.Items.Find(unit.ItemId);
                        item.Amount -= unit.NumberOfItems;
                    }

                    // save changes
                    db.SaveChanges();

                }

                GoBack();

                return true;
            }
            catch (Exception) { err = "حدث خطأ غير متوقع";  return false; }
        }

        public void Clear()
        {
            CustomerId = null;
            Address = "";
            invoiceDate = DateTime.Today;
            Notes = "";
            Total = 0;
            NewUnitItemId = null;
            NewUnitNumberOfItems = 0;
            NewUnitTotal = 0;
            InvoiceUnits.Clear();
        }


        public bool Delete(out string err)
        {
            err = "";
            try
            {
                int id = int.Parse(InvoiceId);
                using(FilterCompanyDataContext db = new FilterCompanyDataContext())
                {
                    MaintenanceInvoice invoice = db.MaintenanceInvoices.Find(id);
                    // add number of items to store for each invoice unit
                    foreach (var unit in invoice.MaintenanceInvoiceUnits)
                        unit.Item.Amount += unit.NumberOfItems;

                    db.SaveChanges();

                    db.MaintenanceInvoices.Remove(invoice);
                    db.SaveChanges();
                }
                GoBack();
                return true;
            }
            catch (Exception) { err = "حدث خطأ غير متوقع"; return false; }
        }


        public void GoBack()
        {
            Clear();
            Mode = false;
        }


        private void LoadCustomers()
        {
            Customers.Clear();
            using (FilterCompanyDataContext db = new FilterCompanyDataContext())
            {
                var customersList = db.Customers.ToList();
                foreach (var customer in customersList)
                    Customers.Add(customer);
            }
        }

        private void LoadItems()
        {
            Items.Clear();
            using(FilterCompanyDataContext db = new FilterCompanyDataContext())
            {
                var itemsList = db.Items.ToList();
                foreach (var item in itemsList)
                    Items.Add(item);
            }
        }

    }
}
