﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using MAIN.Models;

namespace MAIN.ViewModels
{
    public class CustomerEditViewModel : INotifyPropertyChanged
    {

        public CustomerEditViewModel()
        {
            Regions = new ObservableCollection<Region>();
            SubRegions = new ObservableCollection<SubRegion>();
            Streets = new ObservableCollection<Street>();
            Days = new ObservableCollection<int>();

            Days.Clear();
            for (int i = 1; i <= 60; i++)
                Days.Add(i);

            LoadRegions();
        }




        public ObservableCollection<Region> Regions { get; set; }

        public ObservableCollection<SubRegion> SubRegions { get; set; }

        public ObservableCollection<Street> Streets { get; set; }

        public ObservableCollection<int> Days { get; set; }

        private int _code;
        public int Code
        {
            get { return _code; }
            set
            {
                if (_code != value)
                {
                    _code = value;
                    NotifyPropertyChanged("Code");
                }
            }
        }


        private string _fullName;
        public string FullName
        {
            get { return _fullName; }
            set
            {
                if (_fullName != value)
                {
                    _fullName = value;
                    NotifyPropertyChanged("FullName");
                }
            }
        }

        private string _mobile;
        public string Mobile
        {
            get { return _mobile; }
            set
            {
                if (_mobile != value)
                {
                    _mobile = value;
                    NotifyPropertyChanged("Mobile");
                }
            }
        }

        private string _telephone;
        public string Telephone
        {
            get { return _telephone; }
            set
            {
                if (_telephone != value)
                {
                    _telephone = value;
                    NotifyPropertyChanged("Telephone");
                }
            }
        }

        private string _fullAddress;
        public string FullAddress
        {
            get { return _fullAddress; }
            set
            {
                if (_fullAddress != value)
                {
                    _fullAddress = value;
                    NotifyPropertyChanged("FullAddress");
                }
            }
        }

        private int _regionId;
        public int RegionId
        {
            get { return _regionId; }
            set
            {
                if (_regionId != value)
                {
                    _regionId = value;

                    LoadSubRegions();

                    NotifyPropertyChanged("RegionId");
                }
            }
        }

        private int? _subRegionId;
        public int? SubRegionId
        {
            get { return _subRegionId; }
            set
            {
                if (_subRegionId != value)
                {
                    _subRegionId = value;

                    LoadStreets();

                    NotifyPropertyChanged("SubRegionId");
                }
            }
        }



        private int? _streetId;
        public int? StreetId
        {
            get { return _streetId; }
            set
            {
                if (_streetId != value)
                {
                    _streetId = value;
                    NotifyPropertyChanged("StreetId");
                }
            }
        }

        private int? _maintenanceInterval;
        public int? MaintenanceInterval
        {
            get { return _maintenanceInterval; }
            set
            {
                if (_maintenanceInterval != value)
                {
                    _maintenanceInterval = value;
                    NotifyPropertyChanged("MaintenanceInterval");
                }
            }
        }

        private bool _hasMaintenance;
        public bool HasMaintenance
        {
            get { return _hasMaintenance; }
            set
            {
                if (_hasMaintenance != value)
                {
                    _hasMaintenance = value;

                    if (value)
                        MaintenanceInterval = 30; // default
                    else
                        MaintenanceInterval = null;

                    NotifyPropertyChanged("HasMaintenance");
                }
            }
        }

        private bool _mode;
        public bool Mode // 0 for search & 1 for edit
        {
            get { return _mode; }
            set
            {
                if (_mode != value)
                {
                    _mode = value;
                    NotifyPropertyChanged("Mode");
                }
            }
        }


        public void GoBack()
        {
            // switch to search mode
            Mode = false;

            // clear form values
            FullName = "";
            FullAddress = "";
            Mobile = "";
            Telephone = "";
            RegionId = 0;
            HasMaintenance = false;
        }

        public bool Search()
        {

            // search for customer with the given code
            //if (Code == null)
            //    return false;

            //int searchCode = (int)Code;

            using (FilterCompanyDataContext db = new FilterCompanyDataContext())
            {
                Customer result = db.Customers.Find(Code);

                // if found proceed to edit mode and fill form properties, then return true
                if (result != null)
                {
                    FullName = result.Name;
                    Mobile = result.Mobile;
                    Telephone = result.Telephone;
                    FullAddress = result.Address;
                    RegionId = result.Street.SubRegion.RegionId;
                    SubRegionId = result.Street.SubRegionId;
                    StreetId = result.StreetId;

                    if (result.MaintenanceInterval > 0)
                    {
                        HasMaintenance = true;
                        MaintenanceInterval = result.MaintenanceInterval;
                    }
                    else
                    {
                        HasMaintenance = false;
                        MaintenanceInterval = null;
                    }

                    Mode = true;
                    db.Dispose();
                    return true;
                }

                // else return false
                db.Dispose();
                return false;
            }
        }

        public bool Edit()
        {
            using (FilterCompanyDataContext db = new FilterCompanyDataContext())
            {
                try
                {
                    // get customer with the given code
                    if (Code == null) return false;
                    int customerCode = (int)Code;
                    Customer customer = db.Customers.Find(customerCode);

                    // edit customer information
                    customer.Name = FullName;
                    customer.Mobile = Mobile;
                    customer.Address = FullAddress;
                    customer.Telephone = Telephone;
                    customer.StreetId = (int)StreetId;
                    customer.MaintenanceInterval = MaintenanceInterval ?? 0;

                    if (HasMaintenance)
                    {
                        if (customer.NextPaymentDate == null)
                            customer.NextPaymentDate = DateTime.Today.Add(TimeSpan.FromDays((int)MaintenanceInterval));
                    }
                    else
                        customer.NextPaymentDate = null;

                    // save changes to database
                    db.SaveChanges();

                    // go back to search mode & return success
                    db.Dispose();
                    GoBack();
                    return true;
                }
                catch (Exception) { db.Dispose(); return false; }
            }
        }

        public void LoadRegions()
        {
            using (FilterCompanyDataContext db = new FilterCompanyDataContext())
            {
                var regionsFromDb = db.Regions.ToList();
                Regions.Clear();
                foreach (var region in regionsFromDb)
                    Regions.Add(region);
                SubRegionId = null;
            }
        }

        private void LoadSubRegions()
        {
            using (FilterCompanyDataContext db = new FilterCompanyDataContext())
            {
                var subRegionsFromDb = db.SubRegions.Where(s => s.RegionId == RegionId).ToList();
                SubRegions.Clear();
                foreach (var subRegion in subRegionsFromDb)
                    SubRegions.Add(subRegion);
                StreetId = null;
            }
        }

        private void LoadStreets()
        {
            using (FilterCompanyDataContext db = new FilterCompanyDataContext())
            {
                var streetsFromDb = db.Streets.Where(s => s.SubRegionId == SubRegionId).ToList();
                Streets.Clear();
                foreach (var street in streetsFromDb)
                    Streets.Add(street);
            }
        }




        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged(string propertyName)
        {
            if (this.PropertyChanged != null)
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
