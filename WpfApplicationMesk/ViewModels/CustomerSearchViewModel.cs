﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using MAIN.Models;
using System.Data.Entity;
using MAIN.Reports;

namespace MAIN.ViewModels
{
    class CustomerSearchViewModel : INotifyPropertyChanged
    {
        public CustomerSearchViewModel()
        {
            Customers = new ObservableCollection<Customer>();
        }

        public ObservableCollection<Customer> Customers { get; set; }

        private string _code;
        public string Code
        {
            get { return _code; }
            set
            {
                if (_code != value)
                {
                    _code = value;
                    NotifyPropertyChanged("Code");
                }
            }
        }

        private string _name;
        public string Name
        {
            get { return _name; }
            set
            {
                if (_name != value)
                {
                    _name = value;
                    NotifyPropertyChanged("Name");
                }
            }
        }

        private string _mobile;
        public string Mobile
        {
            get { return _mobile; }
            set
            {
                if (_mobile != value)
                {
                    _mobile = value;
                    NotifyPropertyChanged("Mobile");
                }
            }
        }


        public void Search()
        {
            using(FilterCompanyDataContext db=  new FilterCompanyDataContext())
            {
                var query = db.Customers.AsQueryable();

                // filter results
                if (!string.IsNullOrEmpty(Code))
                {
                    int code;
                    if(int.TryParse(Code, out code))
                        query = query.Where(c => c.Id == code);
                }

                if (!string.IsNullOrEmpty(Name))
                    query = query.Where(c => c.Name.ToLower().Trim().Contains(Name.ToLower().Trim()));

                if (!string.IsNullOrEmpty(Mobile))
                    query = query.Where(c => c.Mobile.Trim().Contains(Mobile.Trim()));

                // execute and publish query
                List<Customer> results = query.Include(c => c.Street).Include(c => c.Street.SubRegion).Include(c => c.Street.SubRegion.Region).ToList();
                Customers.Clear();
                foreach (Customer customer in results)
                    Customers.Add(customer);
            }
        }

        public void Clear()
        {
            Customers.Clear();
        }

        public List<CustomerReport> GetReportData()
        {
            return Customers.Select(c => new CustomerReport()
            {
                Id = c.Id,
                Name = c.Name,
                Mobile = c.Mobile,
                Telephone = c.Telephone,
                Address = c.Address,
                MaintenanceInterval = c.MaintenanceInterval
            }).ToList();
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged(string propertyName)
        {
            if (this.PropertyChanged != null)
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
