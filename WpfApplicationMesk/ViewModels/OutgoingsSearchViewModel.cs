﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MAIN.Models;
using System.Collections.ObjectModel;

namespace MAIN.ViewModels
{
    public class OutgoingsSearchViewModel : ViewModelsBase
    {
        public OutgoingsSearchViewModel()
        {
            Types = new ObservableCollection<object>();
            Results = new ObservableCollection<Outgoing>();
            LoadTypes();
            LoadResults();
        }
        public ObservableCollection<object> Types { get; set; }

        public ObservableCollection<Outgoing> Results { get; set; }

        private int selectedType = 0;
        public int SelectedType
        {
            get { return selectedType; }
            set
            {
                selectedType = value;
                LoadResults();
                NotifyPropertyChanged("SelectedType");
            }
        }


        private DateTime fromDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
        public DateTime FromDate
        {
            get { return fromDate; }
            set
            {
                fromDate = value;
                LoadResults();
                NotifyPropertyChanged("FromDate");
            }
        }

        private DateTime toDate = (new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).AddMonths(1).AddDays(-1));
        public DateTime ToDate
        {
            get { return toDate; }
            set
            {
                toDate = value;
                LoadResults();
                NotifyPropertyChanged("ToDate");
            }
        }

        private int total = 0;
        public int Total
        {
            get { return total; }
            set
            {
                total = value;
                NotifyPropertyChanged("Total");
            }
        }

        private void LoadTypes()
        {
            Types.Clear();
            Types.Add(new { Id = 0, Name = "مصروفات داخلية و خارجية" });
            Types.Add(new { Id = 1, Name = "مصروفات داخلية فقط" });
            Types.Add(new { Id = 2, Name = "مصروفات خارجية فقط" });
        }

        private void LoadResults()
        {
            Results.Clear();

            using (FilterCompanyDataContext db = new FilterCompanyDataContext())
            {
                if (SelectedType == 0 || SelectedType == 2)
                {
                    var externalOutgoings = from e in db.ExternalOutgoings
                                           where e.OutgoingDate >= FromDate && e.OutgoingDate <= ToDate
                                           orderby e.OutgoingDate
                                           select new Outgoing
                                           {
                                               Id = e.Id,
                                               Amount = e.Amount * (int)e.Price,
                                               Notes = e.Notes,
                                               OutgoingDate = e.OutgoingDate,
                                               OutgoingType = "مصروفات خارجية"
                                           };

                    foreach (var item in externalOutgoings)
                        Results.Add(item);
                }
                if (SelectedType == 0 || SelectedType == 1)
                {
                    var internalOutgoings = from i in db.InternalOutgoings
                                              where i.OutgoingDate >= FromDate && i.OutgoingDate <= ToDate
                                              orderby i.OutgoingDate
                                              select new Outgoing
                                              {
                                                  Id= i.Id,
                                                  Amount = i.Amount,
                                                  Notes = i.Notes,
                                                  OutgoingDate = i.OutgoingDate,
                                                  OutgoingType = "مصروفات داخلية"
                                              };


                    foreach (var item in internalOutgoings)
                        Results.Add(item);
                }

                //Results = new ObservableCollection<Income>(Results.OrderBy(r => r.IncomeDate));
                Total = Results.Sum(r => r.Amount);
            }
        }

    }
}
