﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MAIN.Models;
using System.Collections.ObjectModel;
using System.Data.Entity;

namespace MAIN.ViewModels
{
    public class MaintenanceSearchViewModel : ViewModelsBase
    {
        public MaintenanceSearchViewModel()
        {
            Customers = new ObservableCollection<Customer>();
            Regions = new ObservableCollection<object>();
            SubRegions = new ObservableCollection<object>();
            Streets = new ObservableCollection<object>();

            LoadCustomers();
            LoadRegions();

        }
        public ObservableCollection<Customer> Customers { get; set; }
        public ObservableCollection<object> Regions { get; set; }
        public ObservableCollection<object> SubRegions { get; set; }

        public ObservableCollection<object> Streets { get; set; }

        private int selectedRegionId;
        public int SelectedRegionId
        {
            get { return selectedRegionId; }
            set
            {

                selectedRegionId = value;

                LoadSubRegions();
                SelectedSubRegionId = 0;

                LoadCustomers();

                NotifyPropertyChanged("SelectedRegionId");

            }
        }

        private int selectedSubRegionId;
        public int SelectedSubRegionId
        {
            get { return selectedSubRegionId; }
            set
            {

                selectedSubRegionId = value;

                LoadStreets();
                SelectedStreetId = 0;

                LoadCustomers();

                NotifyPropertyChanged("SelectedSubRegionId");

            }
        }

        private int selectedStreetId;
        public int SelectedStreetId
        {
            get { return selectedStreetId; }
            set
            {

                selectedStreetId = value;
                LoadCustomers();
                NotifyPropertyChanged("SelectedStreetId");

            }
        }

        private void LoadCustomers()
        {
            using (FilterCompanyDataContext db = new FilterCompanyDataContext())
            {
                var query = db.Customers.Where(c => c.NextPaymentDate != null).Include(c => c.Street).Include(c => c.Street.SubRegion).Include(c => c.Street.SubRegion.Region);

                // filter by region
                if (SelectedRegionId != 0)
                    query = query.Where(c => c.Street.SubRegion.RegionId == SelectedRegionId);

                // filter by subregion
                if (SelectedSubRegionId != 0)
                    query = query.Where(c => c.Street.SubRegionId == SelectedSubRegionId);

                // filter by street
                if (SelectedStreetId != 0)
                    query = query.Where(c => c.StreetId == SelectedStreetId);

                // sort by next payment date
                query = query.OrderBy(c => c.NextPaymentDate);

                // execute query
                var results = query.ToList();

                // add results
                Customers.Clear();
                foreach (var cust in results)
                    Customers.Add(cust);
            }
        }

        private void LoadRegions()
        {
            Regions.Clear();
            Regions.Add(new { Id = 0, Name = "كل المناطق" });
            using (FilterCompanyDataContext db = new FilterCompanyDataContext())
            {
                var regionsList = db.Regions.ToList();
                foreach (var region in regionsList)
                    Regions.Add(new { Id = region.Id, Name = region.Name });
            }
        }

        private void LoadSubRegions()
        {
            SubRegions.Clear();
            SubRegions.Add(new { Id = 0, Name = "كل المناطق الفرعية" });

            if (SelectedRegionId == 0)
                return;

            using (FilterCompanyDataContext db = new FilterCompanyDataContext())
            {
                var subRegionsList = db.SubRegions.Where(s => s.RegionId == SelectedRegionId).ToList();
                foreach (var subRegion in subRegionsList)
                    SubRegions.Add(new { Id = subRegion.Id, Name = subRegion.Name });
            }
        }


        private void LoadStreets()
        {
            Streets.Clear();
            Streets.Add(new { Id = 0, Name = "كل الشوارع" });

            if (SelectedSubRegionId == 0)
                return;

            using (FilterCompanyDataContext db = new FilterCompanyDataContext())
            {
                var streetsList = db.Streets.Where(s => s.SubRegionId == SelectedSubRegionId).ToList();
                foreach (var street in streetsList)
                    Streets.Add(new { Id = street.Id, Name = street.Name });
            }
        }

    }
}
