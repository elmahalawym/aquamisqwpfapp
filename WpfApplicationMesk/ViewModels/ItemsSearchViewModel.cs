﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using MAIN.Models;

namespace MAIN.ViewModels
{
    public class ItemsSearchViewModel : ViewModelsBase
    {
        public ItemsSearchViewModel()
        {
            Items = new ObservableCollection<Item>();
        }

        public ObservableCollection<Item> Items { get; set; }


        private string id;
        public string Id
        {
            get { return id; }
            set
            {
                id = value;
                NotifyPropertyChanged("Id");
            }
        }

        private string name;
        public string Name
        {
            get { return name; }
            set
            {
                name = value;
                NotifyPropertyChanged("Name");
            }
        }

        private string amountFrom;
        public string AmountFrom
        {
            get { return amountFrom; }
            set
            {
                amountFrom = value;
                NotifyPropertyChanged("AmountFrom");
            }
        }

        private string amountTo;
        public string AmountTo
        {
            get { return amountTo; }
            set
            {
                amountTo = value;
                NotifyPropertyChanged("AmountTo");
            }
        }

        public void Search()
        {
            using (FilterCompanyDataContext db = new FilterCompanyDataContext())
            {
                var query = db.Items.Select(i => i);

                if (!string.IsNullOrEmpty(Id))
                {
                    int searchId;
                    if (int.TryParse(Id, out searchId))
                        query = query.Where(i => i.Id == searchId);

                }

                if (!string.IsNullOrEmpty(Name))
                    query = query.Where(i => i.Name.ToLower().Trim().Contains(Name.ToLower().Trim()));


                int searchAmountFrom;
                if (int.TryParse(amountFrom, out searchAmountFrom))
                    query = query.Where(i => i.Amount >= searchAmountFrom);


                int searchAmountTo;
                if (int.TryParse(AmountTo, out searchAmountTo))
                    query = query.Where(i => i.Amount <= searchAmountTo);


                List<Item> results = query.ToList();

                Items.Clear();
                foreach (Item item in results)
                    Items.Add(item);

            }
        }

    }
}
