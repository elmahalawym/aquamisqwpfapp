﻿using MAIN.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAIN.ViewModels
{
    public class InstallmentsAddViewModel : INotifyPropertyChanged
    {
        public InstallmentsAddViewModel()
        {
            Customers = new ObservableCollection<Customer>();
            LoadCustomers();
        }

        public ObservableCollection<Customer> Customers { get; set; }

        private int? _customerId;
        public int? CustomerId
        {
            get { return _customerId; }
            set
            {
                if (_customerId != value)
                {
                    _customerId = value;
                    NotifyPropertyChanged("CustomerId");
                }
            }
        }

        private int _total;
        public int Total
        {
            get { return _total; }
            set
            {
                if (_total != value)
                {
                    _total = value;
                    NotifyPropertyChanged("Total");
                }
            }
        }

        private int _forepart;
        public int Forepart
        {
            get { return _forepart; }
            set
            {
                if (_forepart != value)
                {
                    _forepart = value;
                    NotifyPropertyChanged("Forepart");
                }
            }
        }

        private DateTime _startDate = DateTime.Today;
        public DateTime StartDate
        {
            get
            { return _startDate; }
            set
            {
                _startDate = value;
                NotifyPropertyChanged("StartDate");
            }
        }

        public bool AddInstallment()
        {
            try
            {
                Installment newInstallment = new Installment();
                newInstallment.CustomerId = (int)CustomerId;
                newInstallment.Total = Total;
                newInstallment.InstallmentDate = StartDate;
                newInstallment.PaidAmount = Forepart;

                if (Forepart < Total)
                    newInstallment.NextDate = StartDate.Add(TimeSpan.FromDays(30));
                else
                    newInstallment.NextDate = null;


                using (FilterCompanyDataContext db = new FilterCompanyDataContext())
                {
                    db.Installments.Add(newInstallment);
                    db.SaveChanges();

                    if (Forepart != 0)
                    {
                        InstallmentUnit unit = new InstallmentUnit();
                        unit.InstallmentId = newInstallment.Id;
                        unit.Date = StartDate;
                        unit.Amount = Forepart;

                        db.InstallmentUnits.Add(unit);
                        db.SaveChanges();
                    }
                }

                return true;
            }
            catch (Exception) { return false; }
        }


        public void LoadCustomers()
        {
            using (FilterCompanyDataContext db = new FilterCompanyDataContext())
            {
                var customersFromDb = db.Customers.ToList();
                Customers.Clear();
                foreach (var customer in customersFromDb)
                    Customers.Add(customer);
            }
        }


        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged(string propertyName)
        {
            if (this.PropertyChanged != null)
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
