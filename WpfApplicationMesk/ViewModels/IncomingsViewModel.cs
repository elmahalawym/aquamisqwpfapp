﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MAIN.Models;
using System.Collections.ObjectModel;
using System.Data.Entity;

namespace MAIN.ViewModels
{
    public class IncomingsViewModel : ViewModelsBase
    {
        public IncomingsViewModel()
        {
            IncomingTypes = new ObservableCollection<object>();
            Results = new ObservableCollection<Income>();

            LoadIncomingTypes();
            LoadResults();
        }

        public ObservableCollection<object> IncomingTypes { get; set; }
        public ObservableCollection<Income> Results { get; set; }

        private int selectedType = 0;
        public int SelectedType
        {
            get { return selectedType; }
            set
            {
                selectedType = value;
                LoadResults();
                NotifyPropertyChanged("SelectedType");
            }
        }

        private DateTime fromDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
        public DateTime FromDate
        {
            get { return fromDate; }
            set
            {
                fromDate = value;
                LoadResults();
                NotifyPropertyChanged("FromDate");
            }
        }

        private DateTime toDate = (new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1)).AddMonths(1).AddDays(-1);
        public DateTime ToDate
        {
            get { return toDate; }
            set
            {
                toDate = value;
                LoadResults();
                NotifyPropertyChanged("ToDate");
            }
        }

        private int total = 0;
        public int Total
        {
            get { return total; }
            set
            {
                total = value;
                NotifyPropertyChanged("Total");
            }
        }

        private void LoadResults()
        {
            Results.Clear();

            using (FilterCompanyDataContext db = new FilterCompanyDataContext())
            {
                if (SelectedType == 0 || SelectedType == 2)
                {
                    var installmentUnits = from u in db.InstallmentUnits.Include(i => i.Installment).Include(i => i.Installment.Customer)
                                           where u.Date >= FromDate && u.Date <= ToDate
                                           orderby u.Date
                                           select new Income
                                           {
                                               CustomerName = u.Installment.Customer.Name,
                                               IncomeDate = u.Date,
                                               Type = "قسط شهري",
                                               Value = u.Amount
                                           };

                    foreach (var item in installmentUnits)
                        Results.Add(item);
                }
                if (SelectedType == 0 || SelectedType == 1)
                {
                    var maintenanceInvoices = from i in db.MaintenanceInvoices.Include(i => i.Customer)
                                              where i.InvoiceDate >= FromDate && i.InvoiceDate <= ToDate
                                              orderby i.InvoiceDate
                                              select new Income
                                              {
                                                  CustomerName = i.Customer.Name,
                                                  IncomeDate = i.InvoiceDate,
                                                  Type = "فاتورة صيانة",
                                                  Value = i.TotalAmount
                                              };


                    foreach (var item in maintenanceInvoices)
                        Results.Add(item);
                }

                //Results = new ObservableCollection<Income>(Results.OrderBy(r => r.IncomeDate));
                Total = Results.Sum(r => r.Value);
            }

        }



        private void LoadIncomingTypes()
        {
            IncomingTypes.Add(new { Id = 0, Name = "الصيانة و الأقساط" });
            IncomingTypes.Add(new { Id = 1, Name = "الصيانة فقط" });
            IncomingTypes.Add(new { Id = 2, Name = "الأقساط فقط" });
        }

    }
}
