﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using MAIN.Models;
using System.Data.Entity;

namespace MAIN.ViewModels
{
    public class InstallmentsSearchViewModel : ViewModelsBase
    {
        public InstallmentsSearchViewModel()
        {
            Customers = new ObservableCollection<object>();
            Installments = new ObservableCollection<Installment>();
            LoadCustomers();
        }

        public ObservableCollection<object> Customers { get; set; }

        public ObservableCollection<Installment> Installments { get; set; }

        private int? selectedCustomerId = null;
        public int? SelectedCustomerId
        {
            get { return selectedCustomerId; }
            set
            {
                if(selectedCustomerId != value)
                {
                    selectedCustomerId = value;

                    UpdateInstallments();

                    NotifyPropertyChanged("SelectedCustomerId");
                }
            }
        }


        public void LoadCustomers()
        {
            Customers.Clear();
            Customers.Add(new { Id = 0, Name = "كل العملاء" });
            using(FilterCompanyDataContext db = new FilterCompanyDataContext())
            {
                var customersFromDb = db.Customers.Select(c => new { Id = c.Id, Name = c.Name }).ToList();
                foreach (var customer in customersFromDb)
                    Customers.Add(customer);
            }
        }

        public void UpdateInstallments()
        {
            if (SelectedCustomerId == null)
            {
                Installments.Clear();
                return;

            }
            int customerId = (int)SelectedCustomerId;

            using(FilterCompanyDataContext db = new FilterCompanyDataContext())
            {
                Installments.Clear();

                var installmentsFromDb = db.Installments.Select(i => i).Include(i => i.Customer);
                if (customerId != 0)
                    installmentsFromDb = installmentsFromDb.Where(i => i.CustomerId == customerId);

                List<Installment> installmentsList = installmentsFromDb.ToList();
                foreach (Installment installment in installmentsList)
                    Installments.Add(installment);

            }
        }
    }
}
