﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using MAIN.Models;

namespace MAIN.ViewModels
{
    public class AddCustomerViewModel : INotifyPropertyChanged
    {
        public AddCustomerViewModel()
        {
            Regions = new ObservableCollection<Region>();
            SubRegions = new ObservableCollection<SubRegion>();
            Streets = new ObservableCollection<Street>();
            Days = new ObservableCollection<int>();

            Days.Clear();
            for (int i = 1; i <= 60; i++)
                Days.Add(i);

            LoadRegions();
        }

        private string _fullName;
        public string FullName
        {
            get { return _fullName; }
            set
            {
                if (_fullName != value)
                {
                    _fullName = value;
                    NotifyPropertyChanged("FullName");
                }
            }
        }

        private string _mobile;
        public string Mobile
        {
            get { return _mobile; }
            set
            {
                if (_mobile != value)
                {
                    _mobile = value;
                    NotifyPropertyChanged("Mobile");
                }
            }
        }

        private string _telephone;
        public string Telephone
        {
            get { return _telephone; }
            set
            {
                if (_telephone != value)
                {
                    _telephone = value;
                    NotifyPropertyChanged("Telephone");
                }
            }
        }

        private string _fullAddress;
        public string FullAddress
        {
            get { return _fullAddress; }
            set
            {
                if (_fullAddress != value)
                {
                    _fullAddress = value;
                    NotifyPropertyChanged("FullAddress");
                }
            }
        }


        private int _regionId;
        public int RegionId
        {
            get { return _regionId; }
            set
            {
                if (_regionId != value)
                {
                    _regionId = value;

                    LoadSubRegions();

                    NotifyPropertyChanged("RegionId");
                }
            }
        }


        private int? _subRegionId;
        public int? SubRegionId
        {
            get { return _subRegionId; }
            set
            {
                if (_subRegionId != value)
                {
                    _subRegionId = value;

                    LoadStreets();

                    NotifyPropertyChanged("SubRegionId");
                }
            }
        }


        private int? _streetId;
        public int? StreetId
        {
            get { return _streetId; }
            set
            {
                if (_streetId != value)
                {
                    _streetId = value;
                    NotifyPropertyChanged("StreetId");
                }
            }
        }

        private int? _maintenanceInterval;
        public int? MaintenanceInterval
        {
            get { return _maintenanceInterval; }
            set
            {
                if (_maintenanceInterval != value)
                {
                    _maintenanceInterval = value;
                    NotifyPropertyChanged("MaintenanceInterval");
                }
            }
        }

        private bool _hasMaintenance;
        public bool HasMaintenance
        {
            get { return _hasMaintenance; }
            set
            {
                if (_hasMaintenance != value)
                {
                    _hasMaintenance = value;

                    if (value)
                        MaintenanceInterval = 30; // default
                    else
                        MaintenanceInterval = null;

                    NotifyPropertyChanged("HasMaintenance");
                }
            }
        }

        public ObservableCollection<Region> Regions { get; set; }

        public ObservableCollection<SubRegion> SubRegions { get; set; }

        public ObservableCollection<Street> Streets { get; set; }

        public ObservableCollection<int> Days { get; set; }


        public void LoadRegions()
        {
            using (FilterCompanyDataContext db = new FilterCompanyDataContext())
            {
                var regionsFromDb = db.Regions.ToList();
                Regions.Clear();
                foreach (var region in regionsFromDb)
                    Regions.Add(region);
                SubRegionId = null;
            }
        }

        private void LoadSubRegions()
        {
            using (FilterCompanyDataContext db = new FilterCompanyDataContext())
            {
                var subRegionsFromDb = db.SubRegions.Where(s => s.RegionId == RegionId).ToList();
                SubRegions.Clear();
                foreach (var subRegion in subRegionsFromDb)
                    SubRegions.Add(subRegion);
                StreetId = null;
            }
        }

        private void LoadStreets()
        {
            using (FilterCompanyDataContext db = new FilterCompanyDataContext())
            {
                var streetsFromDb = db.Streets.Where(s => s.SubRegionId == SubRegionId).ToList();
                Streets.Clear();
                foreach (var street in streetsFromDb)
                    Streets.Add(street);
            }
        }

        public bool SaveCustomer()
        {
            try
            {
                using(FilterCompanyDataContext db = new FilterCompanyDataContext())
                {
                    Customer newCustomer = new Customer()
                    {
                        Name = FullName,
                        Mobile = Mobile,
                        Address = FullAddress,
                        Telephone = Telephone,
                        StreetId = (int)StreetId,
                        MaintenanceInterval = MaintenanceInterval ?? 0,
                    };

                    if (HasMaintenance)
                        newCustomer.NextPaymentDate = DateTime.Today.Add(TimeSpan.FromDays((int)MaintenanceInterval));

                    db.Customers.Add(newCustomer);
                    db.SaveChanges();
                }
                return true;
            }
            catch (Exception) { return false; }
        }



        //private bool IsValidate(out string msg)
        //{
        //    if (string.IsNullOrEmpty(FullAddress))
        //        msg = "!خطأ .. من فضلك ادخل الإسم بالكامل";
        //    else if (string.IsNullOrEmpty(Mobile))
        //        msg = "!خطأ .. من فضلك ادخل الموبايل";
        //    else if (!checkNumber(tb_MOBILE_ADD_CUSTOMER.Text.Trim()))
        //        msg = "!خطأ .. من فضلك ادخل رقم موبايل صحيح";
        //    else if (tb_TELEPHONE_ADD_CUSTOMER.Text.Trim() == "")
        //        msg = "!خطأ .. من فضلك ادخل التليفون";
        //    else if (!checkNumber(tb_TELEPHONE_ADD_CUSTOMER.Text.Trim()))
        //        msg = "!خطأ .. من فضلك ادخل رقم تليفون صحيح";
        //    else if (tb_ADDRESS_ADD_CUSTOMER.Text.Trim() == "")
        //        msg = "!خطأ .. من فضلك ادخل العنوان بالكامل";
        //    else if (cb_MAINREGION_ADD_CUSTOMER.SelectedValue == null)
        //        msg = "!خطأ .. من فضلك اختر المنطقة الرئيسية";
        //    else if (cb_SUBREGION_ADD_CUSTOMER.SelectedValue == null)
        //        msg = "!خطأ .. من فضلك اختر المنطقة الفرعية";
        //    else if (cb_STREET_ADD_CUSTOMER.SelectedValue == null)
        //        msg = "!خطأ .. من فضلك اختر إسم الشارع";
        //    else if (!cb_HAVE_MAINTENANCE.IsChecked.Value && cb_MAINTENANCE_RATING.SelectedValue == null)
        //        msg = "!خطأ .. من فضلك اختر معدل الصيانة";
        //    else return true;
        //    return false;
        //}


        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged(string propertyName)
        {
            if (this.PropertyChanged != null)
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

    }
}
