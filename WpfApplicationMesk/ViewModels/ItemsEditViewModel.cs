﻿using MAIN.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAIN.ViewModels
{
    public class ItemsEditViewModel : ViewModelsBase
    {

        private string itemId;
        public string ItemId
        {
            get { return itemId; }
            set
            {
                itemId = value;
                NotifyPropertyChanged("ItemId");
            }
        }

        private string name;
        public string Name
        {
            get { return name; }
            set
            {
                name = value;
                NotifyPropertyChanged("Name");
            }
        }

        private int price;
        public int Price
        {
            get { return price; }
            set
            {
                price = value;
                NotifyPropertyChanged("Price");
            }
        }

        private int amountInStorage;
        public int AmountInStorage
        {
            get { return amountInStorage; }
            set
            {
                amountInStorage = value;
                NotifyPropertyChanged("AmountInStorage");
            }
        }


        private string notes;
        public string Notes
        {
            get { return notes; }
            set
            {
                notes = value;
                NotifyPropertyChanged("Notes");
            }
        }

        private bool mode = false;
        public bool Mode
        {
            get { return mode; }
            set
            {
                mode = value;
                NotifyPropertyChanged("Mode");
            }
        }

        public bool Search()
        {
            int id;
            if (int.TryParse(itemId, out id))
            {
                using (FilterCompanyDataContext db = new FilterCompanyDataContext())
                {
                    Item item = db.Items.Find(id);
                    if (item == null)
                        return false;

                    Name = item.Name;
                    Price = item.Price;
                    AmountInStorage = item.Amount;
                    Notes = item.Notes;

                    Mode = true;
                    return true;
                }
            }
            else
                return false;
        }

        public void GoBack()
        {
            Mode = false;
            Name = "";
            Price = 0;
            AmountInStorage = 0;
            Notes = "";
        }

        public bool Edit(out string err)
        {
            using (FilterCompanyDataContext db = new FilterCompanyDataContext())
            {
                try
                {
                    int id = int.Parse(ItemId);

                    err = "";
                    if (price < 0)
                    {
                        err = "القيمة لا يمكن أن تكون أقل من صفر";
                        return false;
                    }

                    if (price == 0)
                    {
                        err = "القيمة لا يمكن أن تساوي صفر";
                        return false;
                    }

                    if (amountInStorage < 0)
                    {
                        err = "الكمية لا يمكن أن تكون أقل من صفر";
                        return false;
                    }
                    if (db.Items.Where(i => i.Name.ToLower().Trim() == name.ToLower().Trim() && i.Id != id).Count() != 0)
                    {
                        err = "يوجد صنف مسجل بنفس الإسم";
                        return false;
                    }

                    Item item = db.Items.Find(id);
                    item.Name = Name;
                    item.Price = Price;
                    item.Amount = AmountInStorage;
                    item.Notes = Notes;

                    db.SaveChanges();

                    GoBack();
                    return true;
                }
                catch (Exception) { err = "حدث خطأ غير متوقع!"; return false; }
            }

        }

    }
}
