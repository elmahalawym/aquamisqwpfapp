﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MAIN.Models;
using System.Collections.ObjectModel;

namespace MAIN.ViewModels
{
    class ExternalOutgoingsAddViewModel : ViewModelsBase
    {
        public ExternalOutgoingsAddViewModel()
        {
            Items = new ObservableCollection<Item>();
            LoadItems();
        }

        public ObservableCollection<Item> Items { get; set; }

        private int? selectedItemId;
        public int? SelectedItemId
        {
            get { return selectedItemId; }
            set
            {
                selectedItemId = value;
                GetItemPrice();
                NotifyPropertyChanged("SelectedItemId");
            }
        }


        private DateTime outgoingDate = DateTime.Today;
        public DateTime OutgoingDate
        {
            get { return outgoingDate; }
            set
            {
                outgoingDate = value;
                NotifyPropertyChanged("OutgoingDate");
            }
        }

        private int price;
        public int Price
        {
            get { return price; }
            set
            {
                price = value;
                CalculateTotal();
                NotifyPropertyChanged("Price");
            }
        }

        private int amount;
        public int Amount
        {
            get { return amount; }
            set
            {
                amount = value;
                CalculateTotal();
                NotifyPropertyChanged("Amount");
            }
        }

        private int total;
        public int Total
        {
            get { return total; }
            private set
            {
                total = value;
                NotifyPropertyChanged("Total");
            }
        }

        private string notes;
        public string Notes
        {
            get { return notes; }
            set
            {
                notes = value;
                NotifyPropertyChanged("Notes");
            }
        }

        public bool Save(out string err)
        {
            err = "";
            if(Price <= 0)
            {
                err = "السعر لا يمكن أن يكون صفر أو أقل";
                return false;
            }

            if (Amount <= 0)
            {
                err = "الكمية لا يمكن أن تكون صفر أو أقل";
                return false;
            }

            try
            {
                using (FilterCompanyDataContext db = new FilterCompanyDataContext())
                {
                    Item selectedItem = db.Items.Find(SelectedItemId);
                    if (selectedItem == null)
                    {
                        err = "برجاء اختيار الصنف";
                        return false;
                    }

                    // add new ExternalOutgoing
                    ExternalOutgoing outgoing = new ExternalOutgoing();
                    outgoing.ItemId = (int)SelectedItemId;
                    outgoing.OutgoingDate = OutgoingDate;
                    outgoing.Price = Price;
                    outgoing.Amount = Amount;
                    outgoing.Notes = Notes;
                    db.ExternalOutgoings.Add(outgoing);

                    // add the amount of new items in store
                    selectedItem.Amount += Amount;

                    // save changes
                    db.SaveChanges();

                    Clear();
                    return true;

                }
            }
            catch (Exception) { err = "حدث خطأ غير متوقع!"; return false; }
        }



        public void Clear()
        {
            SelectedItemId = null;
            OutgoingDate = DateTime.Today;
            Price = 0;
            Amount = 0;
            Total = 0;
            Notes = "";
        }

        private void CalculateTotal()
        {
            Total = Price * Amount;
        }

        private void LoadItems()
        {
            Items.Clear();
            using (FilterCompanyDataContext db = new FilterCompanyDataContext())
            {
                var items = db.Items.ToList();
                foreach (var item in items)
                    Items.Add(item);
            }
        }


        private void GetItemPrice()
        {
            if (SelectedItemId == null)
                return;

            using(FilterCompanyDataContext db = new FilterCompanyDataContext())
            {
                Item item = db.Items.Find(SelectedItemId);
                if (item == null)
                    return;

                Price = item.Price;
            }
        }

    }
}
