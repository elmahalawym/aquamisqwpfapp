﻿using MAIN.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAIN.ViewModels
{
    public class InternalOutgoingsAddViewModel : ViewModelsBase
    {


        private DateTime outgoingDate = DateTime.Today;
        public DateTime OutGoingDate
        {
            get { return outgoingDate; }
            set
            {
                outgoingDate = value;
                NotifyPropertyChanged("OutgoingDate");
            }
        }

        private int outgoingValue = 0;
        public int Value
        {
            get { return outgoingValue; }
            set
            {
                outgoingValue = value;
                NotifyPropertyChanged("Value");
            }
        }

        private string notes;
        public string Notes
        {
            get { return notes; }
            set
            {
                notes = value;
                NotifyPropertyChanged("Notes");
            }
        }


        public bool Save(out string err)
        {
            err = "";
            if(Value <= 0)
            {
                err = "القيمة لا يمكن أن تكون صفر أو أقل";
                return false;
            }

            try
            {
                using(FilterCompanyDataContext db = new FilterCompanyDataContext())
                {
                    db.InternalOutgoings.Add(new InternalOutgoing()
                    {
                        Amount = Value,
                        OutgoingDate = OutGoingDate,
                        Notes = Notes
                    });
                    db.SaveChanges();
                    Clear();
                    return true;
                }
            }
            catch (Exception) { err = "حدث خطأ غير متوقع!"; return false; }
        }

        public void Clear()
        {
            OutGoingDate = DateTime.Today;
            notes = "";
            Value = 0;
        }
    }
}
