﻿using MAIN.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAIN.ViewModels
{
    public class InternalOutgoingsEditViewModel : ViewModelsBase
    {

        private string code;
        public string Code
        {
            get { return code; }
            set
            {
                code = value;
                NotifyPropertyChanged("Code");
            }
        }


        private DateTime outgoingDate;
        public DateTime OutGoingDate
        {
            get { return outgoingDate; }
            set
            {
                outgoingDate = value;
                NotifyPropertyChanged("OutgoingDate");
            }
        }

        private int outgoingValue = 0;
        public int Value
        {
            get { return outgoingValue; }
            set
            {
                outgoingValue = value;
                NotifyPropertyChanged("Value");
            }
        }

        private string notes;
        public string Notes
        {
            get { return notes; }
            set
            {
                notes = value;
                NotifyPropertyChanged("Notes");
            }
        }

        private bool mode;
        public bool Mode
        {
            get { return mode; }
            set
            {
                mode = value;
                NotifyPropertyChanged("Mode");
            }
        }


        public bool Search()
        {
            int id;
            if (!int.TryParse(Code, out id))
                return false;


            using (FilterCompanyDataContext db = new FilterCompanyDataContext())
            {
                InternalOutgoing internalOutgoing = db.InternalOutgoings.Find(id);
                if (internalOutgoing == null)
                    return false;

                OutGoingDate = internalOutgoing.OutgoingDate;
                Value = internalOutgoing.Amount;
                Notes = internalOutgoing.Notes;
                Mode = true;
            }

            return true;

        }


        public bool SaveChanges(out string err)
        {
            err = "";
            if (Value <= 0)
            {
                err = "القيمة لا يمكن أن تكون صفر أو أقل";
                return false;
            }

            int id;
            if (!int.TryParse(Code, out id))
                return false;

            try
            {
                using (FilterCompanyDataContext db = new FilterCompanyDataContext())
                {
                    InternalOutgoing internalOutgoing = db.InternalOutgoings.Find(id);
                    if (internalOutgoing == null)
                    {
                        err = "حدث خطأ غير متوقع!";
                        return false; 
                    }

                    internalOutgoing.Notes = Notes;
                    internalOutgoing.OutgoingDate = OutGoingDate;
                    internalOutgoing.Amount = Value;

                    db.SaveChanges();
                    GoBack();
                    return true;
                }
            }
            catch (Exception) { err = "حدث خطأ غير متوقع!"; return false; }
        }

        public bool Delete()
        {
            int id;
            if (!int.TryParse(Code, out id))
                return false;

            try
            {
                using (FilterCompanyDataContext db = new FilterCompanyDataContext())
                {
                    InternalOutgoing internalOutgoing = db.InternalOutgoings.Find(id);
                    db.InternalOutgoings.Remove(internalOutgoing);
                    db.SaveChanges();

                    GoBack();
                    return true;
                }
            }
            catch (Exception) { return false; }


        }

        public void GoBack()
        {
            OutGoingDate = DateTime.Today;
            Notes = "";
            Value = 0;
            Mode = false;
        }
    }
}
