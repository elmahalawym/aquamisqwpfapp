﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using MAIN.Models;

namespace MAIN.ViewModels
{
    public class ExternalOutgoingsEditViewModel : ViewModelsBase
    {
        public ExternalOutgoingsEditViewModel()
        {
            Items = new ObservableCollection<Item>();
            LoadItems();
        }

        public ObservableCollection<Item> Items { get; set; }

        private string code;
        public string Code
        {
            get { return code; }
            set
            {
                code = value;
                NotifyPropertyChanged("Code");
            }
        }

        private int? selectedItemId;
        public int? SelectedItemId
        {
            get { return selectedItemId; }
            set
            {
                selectedItemId = value;
                GetItemPrice();
                NotifyPropertyChanged("SelectedItemId");
            }
        }


        private DateTime outgoingDate = DateTime.Today;
        public DateTime OutgoingDate
        {
            get { return outgoingDate; }
            set
            {
                outgoingDate = value;
                NotifyPropertyChanged("OutgoingDate");
            }
        }

        private int price;
        public int Price
        {
            get { return price; }
            set
            {
                price = value;
                CalculateTotal();
                NotifyPropertyChanged("Price");
            }
        }

        private int amount;
        public int Amount
        {
            get { return amount; }
            set
            {
                amount = value;
                CalculateTotal();
                NotifyPropertyChanged("Amount");
            }
        }

        private int total;
        public int Total
        {
            get { return total; }
            private set
            {
                total = value;
                NotifyPropertyChanged("Total");
            }
        }

        private string notes;
        public string Notes
        {
            get { return notes; }
            set
            {
                notes = value;
                NotifyPropertyChanged("Notes");
            }
        }

        private bool mode;
        public bool Mode
        {
            get { return mode; }
            set
            {
                mode = value;
                NotifyPropertyChanged("Mode");
            }
        }

        public bool Save(out string err)
        {
            err = "";
            if (Price <= 0)
            {
                err = "السعر لا يمكن أن يكون صفر أو أقل";
                return false;
            }

            if (Amount <= 0)
            {
                err = "الكمية لا يمكن أن تكون صفر أو أقل";
                return false;
            }

            int id;
            if (!int.TryParse(Code, out id))
            {
                err = "خطأ غير متوقع";
                return false;
            }

            try
            {
                using (FilterCompanyDataContext db = new FilterCompanyDataContext())
                {
                    Item selectedItem = db.Items.Find(SelectedItemId);
                    if (selectedItem == null)
                    {
                        err = "برجاء اختيار الصنف";
                        return false;
                    }

                    // add new ExternalOutgoing
                    ExternalOutgoing outgoing = db.ExternalOutgoings.Find(id);

                    int amountDiff = Amount - outgoing.Amount;

                    outgoing.ItemId = (int)SelectedItemId;
                    outgoing.OutgoingDate = OutgoingDate;
                    outgoing.Price = Price;
                    outgoing.Amount = Amount;
                    outgoing.Notes = Notes;

                    // add the amount of new items in store
                    selectedItem.Amount += amountDiff;

                    // save changes
                    db.SaveChanges();

                    GoBack();
                    return true;

                }
            }
            catch (Exception) { err = "حدث خطأ غير متوقع!"; return false; }
        }

        public bool Delete()
        {
            int id;
            if (!int.TryParse(Code, out id))
                return false;

            try
            {
                using (FilterCompanyDataContext db = new FilterCompanyDataContext())
                {
                    ExternalOutgoing externalOutgoing = db.ExternalOutgoings.Find(id);
                    if (externalOutgoing == null)
                        return false;

                    Item item = externalOutgoing.Item;
                    item.Amount -= externalOutgoing.Amount;

                    db.ExternalOutgoings.Remove(externalOutgoing);
                    db.SaveChanges();

                    GoBack();
                    return true;
                }
            }
            catch (Exception) { return false; }
        }


        public bool Search()
        {
            int id;
            if (!int.TryParse(Code, out id))
                return false;

            using(FilterCompanyDataContext db = new FilterCompanyDataContext())
            {
                ExternalOutgoing externaloutgoing = db.ExternalOutgoings.Find(id);
                if (externaloutgoing == null)
                    return false;

                Mode = true;

                SelectedItemId = externaloutgoing.ItemId;
                OutgoingDate = externaloutgoing.OutgoingDate;
                Price = (int)externaloutgoing.Price;
                Amount = externaloutgoing.Amount;
                CalculateTotal();
                Notes = externaloutgoing.Notes;
                return true;

            }
        }


        public void Clear()
        {
            SelectedItemId = null;
            OutgoingDate = DateTime.Today;
            Price = 0;
            Amount = 0;
            Total = 0;
            Notes = "";
        }

        public void GoBack()
        {
            Clear();
            Mode = false;
        }

        private void CalculateTotal()
        {
            Total = Price * Amount;
        }

        private void LoadItems()
        {
            Items.Clear();
            using (FilterCompanyDataContext db = new FilterCompanyDataContext())
            {
                var items = db.Items.ToList();
                foreach (var item in items)
                    Items.Add(item);
            }
        }


        private void GetItemPrice()
        {
            if (SelectedItemId == null)
                return;

            using (FilterCompanyDataContext db = new FilterCompanyDataContext())
            {
                Item item = db.Items.Find(SelectedItemId);
                if (item == null)
                    return;

                Price = item.Price;
            }
        }
    }
}
