﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MAIN.Models;
using System.Collections.ObjectModel;

namespace MAIN.ViewModels
{
    public class InstallmentDetailsViewModel : ViewModelsBase
    {
        private int installmentId;

        public InstallmentDetailsViewModel(int installmentId)
        {
            InstallmentUnits = new ObservableCollection<InstallmentUnit>();
            this.installmentId = installmentId;

            LoadInstallmentDetails();
        }
        public ObservableCollection<InstallmentUnit> InstallmentUnits { get; set; }

        private int _customerId;
        public int CustomerId
        {
            get { return _customerId; }
            set
            {
                if (_customerId != value)
                {
                    _customerId = value;
                    NotifyPropertyChanged("CustomerId");
                }
            }
        }

        private string _customerName;
        public string CustomerName
        {
            get { return _customerName; }
            set
            {
                if (_customerName != value)
                {
                    _customerName = value;
                    NotifyPropertyChanged("CustomerName");
                }
            }
        }

        private int _total;
        public int Total
        {
            get { return _total; }
            set
            {
                if (_total != value)
                {
                    _total = value;
                    NotifyPropertyChanged("Total");
                }
            }
        }

        private int _paidAmount;
        public int PaidAmount
        {
            get { return _paidAmount; }
            set
            {
                if (_paidAmount != value)
                {
                    _paidAmount = value;
                    NotifyPropertyChanged("PaidAmount");
                }
            }
        }

        private int _remaining;
        public int Remaining
        {
            get { return _remaining; }
            set
            {
                if (_remaining != value)
                {
                    _remaining = value;
                    NotifyPropertyChanged("Remaining");
                }
            }
        }

        private DateTime _startDate;
        public DateTime StartDate
        {
            get { return _startDate; }
            set
            {
                if (_startDate != value)
                {
                    _startDate = value;
                    NotifyPropertyChanged("StartDate");
                }
            }
        }

        private DateTime? _nextDate;
        public DateTime? NextDate
        {
            get { return _nextDate; }
            set
            {
                if (_nextDate != value)
                {
                    _nextDate = value;
                    NotifyPropertyChanged("NextDate");
                }
            }
        }

        private int _newUnitAmount;
        public int NewUnitAmount
        {
            get { return _newUnitAmount; }
            set
            {
                if (_newUnitAmount != value)
                {
                    _newUnitAmount = value;
                    NotifyPropertyChanged("NewUnitAmount");
                }
            }
        }

        private DateTime _newUnitDate = DateTime.Today;
        public DateTime NewUnitDate
        {
            get { return _newUnitDate; }
            set
            {
                if (_newUnitDate != value)
                {
                    _newUnitDate = value;
                    NotifyPropertyChanged("NewUnitDate");
                }
            }
        }

        public bool AddNewUnit(out string err)
        {
            err = "";

            if ((NewUnitAmount + PaidAmount > Total))
            {
                err = "لا يمكن أن تكون القيمة المدفوعة أعلي من القيمة الكلية!";
                return false;
            }

            if (NewUnitAmount <= 0)
            {
                err = "من فضلك أدخ قيمة صحيحة";
                return false;
            }

            if (NewUnitDate < StartDate)
            {
                err = "ميعاد الدفعة لا يمكن أن يكون قبل ميعاد بدء القسط!";
                return false;
            }
            try
            {
                using (FilterCompanyDataContext db = new FilterCompanyDataContext())
                {
                    InstallmentUnit newUnit = new InstallmentUnit();
                    newUnit.InstallmentId = this.installmentId;
                    newUnit.Amount = NewUnitAmount;
                    newUnit.Date = NewUnitDate;

                    db.InstallmentUnits.Add(newUnit);
                    db.SaveChanges();

                    Installment installment = db.Installments.Find(installmentId);
                    //installment.PaidAmount = installment.PaidAmount + newUnit.Amount;
                    installment.PaidAmount = installment.InstallmentUnits.Sum(u => u.Amount);

                    if (installment.PaidAmount < installment.Total)
                    {
                        // calculate new date
                        int nUnits = installment.InstallmentUnits.Count;
                        DateTime nextDate = installment.InstallmentDate.Add(TimeSpan.FromDays(30 * nUnits));
                        installment.NextDate = nextDate;
                    }
                    else installment.NextDate = null;

                    db.SaveChanges();

                    return true;
                }
            }
            catch (Exception) { return false; }
        }

        private void LoadInstallmentDetails()
        {
            using (FilterCompanyDataContext db = new FilterCompanyDataContext())
            {
                Installment installment = db.Installments.Find(installmentId);
                if (installment != null)
                {
                    CustomerId = installment.CustomerId;
                    CustomerName = installment.Customer.Name;
                    Total = installment.Total;
                    PaidAmount = installment.PaidAmount;
                    Remaining = installment.Total - installment.PaidAmount;
                    StartDate = installment.InstallmentDate;
                    NextDate = installment.NextDate;

                    InstallmentUnits.Clear();
                    foreach (var unit in installment.InstallmentUnits)
                        InstallmentUnits.Add(unit);
                }
            }
        }

    }
}
