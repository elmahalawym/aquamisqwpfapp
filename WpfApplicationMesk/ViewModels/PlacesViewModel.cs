﻿using MAIN.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Collections.ObjectModel;

namespace MAIN.ViewModels
{
    public class PlacesViewModel : INotifyPropertyChanged
    {
        //private FilterCompanyDataContext db;

        public PlacesViewModel()
        {
            // initialization
            //db = new FilterCompanyDataContext();
            Regions = new ObservableCollection<Region>();
            SubRegions = new ObservableCollection<SubRegion>();
            DeleteSubRegion_RegionsList = new ObservableCollection<SubRegion>();
            NewStreet_SubRegionsList = new ObservableCollection<SubRegion>();
            DeleteStreet_SubRegionsList = new ObservableCollection<SubRegion>();
            DeleteStreet_StreetList = new ObservableCollection<Street>();

            // load data
            LoadRegions();
        }

        public void LoadRegions()
        {
            try
            {
                using (FilterCompanyDataContext db = new FilterCompanyDataContext())
                {
                    List<Region> regionsFromDatabase = db.Regions.ToList();
                    Regions.Clear();
                    foreach (Region region in regionsFromDatabase)
                        Regions.Add(region);
                }
            }
            catch (Exception) { }
        }

        private void LoadSubRegions()
        {
            try
            {
                using (FilterCompanyDataContext db = new FilterCompanyDataContext())
                {
                    List<SubRegion> subRegionsFromDatabase = db.SubRegions.ToList();
                    SubRegions.Clear();
                    foreach (SubRegion subRegion in subRegionsFromDatabase)
                        SubRegions.Add(subRegion);
                }
            }
            catch (Exception) { }
        }

        #region Properties

        public ObservableCollection<Region> Regions { get; set; }
        public ObservableCollection<SubRegion> SubRegions { get; set; }

        public ObservableCollection<SubRegion> DeleteSubRegion_RegionsList { get; set; }

        public ObservableCollection<SubRegion> NewStreet_SubRegionsList { get; set; }

        public ObservableCollection<SubRegion> DeleteStreet_SubRegionsList { get; set; }

        public ObservableCollection<Street> DeleteStreet_StreetList { get; set; }

        private string _newRegionName = "";
        public string NewRegionName
        {
            get
            {
                return _newRegionName;
            }
            set
            {
                if (_newRegionName != value)
                {
                    _newRegionName = value;
                    NotifyPropertyChanged("NewRegionName");
                }
            }
        }

        private string _newSubRegionName = "";
        public string NewSubRegionName
        {
            get
            {
                return _newSubRegionName;
            }
            set
            {
                if (_newSubRegionName != value)
                {
                    _newSubRegionName = value;
                    NotifyPropertyChanged("NewSubRegionName");
                }
            }
        }

        private int _newSubRegion_RegionId;
        public int NewSubRegion_RegionId
        {
            get { return _newSubRegion_RegionId; }
            set
            {
                if (_newSubRegion_RegionId != value)
                {
                    _newSubRegion_RegionId = value;
                    NotifyPropertyChanged("NewSubRegion_RegionId");
                }
            }
        }

        private int _deleteSubRegion_RegionId;
        public int DeleteSubRegion_RegionId
        {
            get { return _deleteSubRegion_RegionId; }
            set
            {
                if (_deleteSubRegion_RegionId != value)
                {
                    _deleteSubRegion_RegionId = value;

                    DeleteSubRegion_RegionsList.Clear();
                    using (FilterCompanyDataContext db = new FilterCompanyDataContext())
                    {
                        List<SubRegion> subRegionsForSelectedRegion = db.SubRegions.Where(s => s.RegionId == value).ToList();
                        foreach (SubRegion subRegion in subRegionsForSelectedRegion)
                            DeleteSubRegion_RegionsList.Add(subRegion);
                    }
                    NotifyPropertyChanged("DeleteSubRegion_RegionId");
                }
            }
        }


        private int _newStreet_RegionId;
        public int NewStreet_RegionId
        {
            get { return _newStreet_RegionId; }
            set
            {
                if (_newStreet_RegionId != value)
                {
                    _newStreet_RegionId = value;

                    NewStreet_SubRegionsList.Clear();
                    using (FilterCompanyDataContext db = new FilterCompanyDataContext())
                    {
                        List<SubRegion> subRegions = db.SubRegions.Where(s => s.RegionId == value).ToList();
                        foreach (SubRegion subRegion in subRegions)
                            NewStreet_SubRegionsList.Add(subRegion);
                    }
                    NotifyPropertyChanged("NewStreet_RegionId");
                }
            }
        }

        private int _newStreet_SubRegionId;
        public int NewStreet_SubRegionId
        {
            get { return _newStreet_SubRegionId; }
            set
            {
                if (_newStreet_SubRegionId != value)
                {
                    _newStreet_SubRegionId = value;
                    NotifyPropertyChanged("NewStreet_SubRegionId");
                }
            }
        }

        private string _newStreetName = "";
        public string NewStreetName
        {
            get { return _newStreetName; }
            set
            {
                if (_newStreetName != value)
                {
                    _newStreetName = value;
                    NotifyPropertyChanged("NewStreetName");
                }
            }
        }


        private int _deleteStreet_RegionId;
        public int DeleteStreet_RegionId
        {
            get { return _deleteStreet_RegionId; }
            set
            {
                if (_deleteStreet_RegionId != value)
                {
                    _deleteStreet_RegionId = value;

                    DeleteStreet_SubRegionsList.Clear();
                    using (FilterCompanyDataContext db = new FilterCompanyDataContext())
                    {
                        List<SubRegion> subRegions = db.SubRegions.Where(s => s.RegionId == value).ToList();
                        foreach (SubRegion subRegion in subRegions)
                            DeleteStreet_SubRegionsList.Add(subRegion);
                    }
                    NotifyPropertyChanged("DeleteStreet_RegionId");
                }
            }
        }

        private int _deleteStreet_SubRegionId;
        public int DeleteStreet_SubRegionId
        {
            get { return _deleteStreet_SubRegionId; }
            set
            {
                if (_deleteStreet_SubRegionId != value)
                {
                    _deleteStreet_SubRegionId = value;

                    DeleteStreet_StreetList.Clear();
                    using (FilterCompanyDataContext db = new FilterCompanyDataContext())
                    {
                        List<Street> streets = db.Streets.Where(s => s.SubRegionId == value).ToList();
                        foreach (var street in streets)
                            DeleteStreet_StreetList.Add(street);
                    }
                    NotifyPropertyChanged("DeleteStreet_SubRegionId");
                }
            }
        }

        private int _deleteStreet_StreetId;
        public int DeleteStreet_StreetId
        {
            get { return _deleteStreet_StreetId; }
            set
            {
                if (_deleteStreet_StreetId != value)
                {
                    _deleteStreet_StreetId = value;
                    NotifyPropertyChanged("DeleteStreet_StreetId");
                }
            }
        }


        #endregion


        public bool SaveNewRegion()
        {
            using (FilterCompanyDataContext db = new FilterCompanyDataContext())
            {
                if (string.IsNullOrEmpty(NewRegionName) || db.Regions.Where(r => r.Name.ToLower().Trim() == NewRegionName.ToLower().Trim()).Count() > 0)
                    return false;

                // save new region
                try
                {
                    db.Regions.Add(new Region() { Name = NewRegionName });
                    db.SaveChanges();

                    // refill regions list
                    LoadRegions();

                    return true;
                }
                catch (Exception) { return false; }
            }
        }

        public bool CanRemoveRegion(Region region)
        {
            using (FilterCompanyDataContext db = new FilterCompanyDataContext())
                return db.SubRegions.Where(s => s.RegionId == region.Id).Count() == 0;
        }

        public bool RemoveRegion(Region region)
        {
            try
            {
                using (FilterCompanyDataContext db = new FilterCompanyDataContext())
                {
                    Region r = db.Regions.Find(region.Id);
                    db.Regions.Remove(r);
                    db.SaveChanges();

                    // refill regions list
                    LoadRegions();
                }
                return true;
            }
            catch (Exception) { return false; }
        }

        public bool SaveNewSubRegion()
        {
            using (FilterCompanyDataContext db = new FilterCompanyDataContext())
            {
                if (string.IsNullOrEmpty(NewSubRegionName) || db.SubRegions.Where(r => r.Name.ToLower().Trim() == NewSubRegionName.ToLower().Trim() && r.RegionId == NewSubRegion_RegionId).Count() > 0)
                    return false;

                // save new sub region
                try
                {

                    db.SubRegions.Add(new SubRegion() { Name = NewSubRegionName, RegionId = NewSubRegion_RegionId });
                    db.SaveChanges();

                    // refill regions list
                    LoadSubRegions();

                    return true;
                }
                catch (Exception) { return false; }
            }
        }

        public bool CanRemoveSubRegion(SubRegion subRegion)
        {
            using (FilterCompanyDataContext db = new FilterCompanyDataContext())
                return db.Streets.Where(s => s.SubRegionId == subRegion.Id).Count() == 0;
        }

        public bool RemoveSubRegion(SubRegion subRegion)
        {
            try
            {
                using (FilterCompanyDataContext db = new FilterCompanyDataContext())
                {
                    db.SubRegions.Remove(db.SubRegions.Find(subRegion.Id));
                    db.SaveChanges();
                }
                // refill sub regions list
                LoadSubRegions();

                return true;
            }
            catch (Exception) { return false; }
        }

        public bool SaveNewStreet()
        {
            using (FilterCompanyDataContext db = new FilterCompanyDataContext())
            {
                if (string.IsNullOrEmpty(NewStreetName) || db.Streets.Where(s => s.Name.ToLower().Trim() == NewStreetName.ToLower().Trim() && s.SubRegionId == NewStreet_SubRegionId).Count() > 0)
                    return false;

                // save new street
                try
                {

                    db.Streets.Add(new Street() { Name = NewStreetName, SubRegionId = NewStreet_SubRegionId });
                    db.SaveChanges();

                    return true;
                }
                catch (Exception) { return false; }
            }
        }

        public bool RemoveStreet()
        {
            try
            {
                using (FilterCompanyDataContext db = new FilterCompanyDataContext())
                {
                    Street selectedStreet = db.Streets.Find(DeleteStreet_StreetId);
                    db.Streets.Remove(selectedStreet);
                    db.SaveChanges();

                    // clear comboboxes
                    DeleteStreet_StreetList.Clear();
                    var streets = db.Streets.Where(s => s.SubRegionId == DeleteStreet_SubRegionId).ToList();
                    foreach (var street in streets)
                        DeleteStreet_StreetList.Add(street);
                }
                return true;
            }
            catch (Exception) { return false; }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged(string propertyName)
        {
            if (this.PropertyChanged != null)
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
