﻿using MAIN.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAIN.ViewModels
{
    public class ItemsAddViewModel : ViewModelsBase
    {
        private string name;
        public string Name
        {
            get { return name; }
            set
            {
                name = value;
                NotifyPropertyChanged("Name");
            }
        }

        private int price;
        public int Price
        {
            get { return price; }
            set
            {
                price = value;
                NotifyPropertyChanged("Price");
            }
        }

        private int amountInStorage;
        public int AmountInStorage
        {
            get { return amountInStorage; }
            set
            {
                amountInStorage = value;
                NotifyPropertyChanged("AmountInStorage");
            }
        }


        private string notes;
        public string Notes
        {
            get { return notes; }
            set
            {
                notes = value;
                NotifyPropertyChanged("Notes");
            }
        }

        public bool AddItem(out string err)
        {
            using (FilterCompanyDataContext db = new FilterCompanyDataContext())
            {
                err = "";
                if (price < 0)
                {
                    err = "القيمة لا يمكن أن تكون أقل من صفر";
                    return false;
                }

                if (price == 0)
                {
                    err = "القيمة لا يمكن أن تساوي صفر";
                    return false;
                }

                if (amountInStorage < 0)
                {
                    err = "الكمية لا يمكن أن تكون أقل من صفر";
                    return false;
                }

                if(db.Items.Where(i => i.Name.ToLower().Trim() == name.ToLower().Trim()).Count() != 0)
                {
                    err = "يوجد صنف مسجل بنفس الإسم";
                    return false;
                }

                try
                {
                    db.Items.Add(new Item
                    {
                        Name = name,
                        Price = price,
                        Amount = amountInStorage,
                        Notes = notes
                    });
                    db.SaveChanges();

                    Name = "";
                    Price = 0;
                    AmountInStorage = 0;
                    Notes = "";

                    return true;

                }
                catch (Exception) { return true; }
            }
        }
    }
}
