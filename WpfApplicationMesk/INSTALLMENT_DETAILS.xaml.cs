﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System.Text.RegularExpressions;

namespace MAIN
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class Window1 : Window
    {
        string customerID = "";
        string customerName = "";
        string installmentID = "";
        string remainder = "";

        public Window1(String ID, String name, String instID, String remain)
        {
            InitializeComponent();

            customerID = ID;
            customerName = name;
            installmentID = instID;
            remainder = remain;

            tb_CUSTOMER_ID_INSTALLMENT_DETAILS.Text = customerID;
            tb_CUSTOMER_NAME_INSTALLMENT_DETAILS.Text = customerName;

            BindingOperations.ClearAllBindings(dg_SHOW_INSTALLMENT_DETAILS);


            getInstallmentDetails(instID, dg_SHOW_INSTALLMENT_DETAILS);
        }


        public void getInstallmentDetails(string installmentID, DataGrid dg)
        {
            string ConString = ConfigurationManager.ConnectionStrings["ConString"].ConnectionString;
            string CmdString = string.Empty;
            using (SqlConnection con = new SqlConnection(ConString))
            {
                SqlCommand cmd = con.CreateCommand();
                cmd.CommandType = CommandType.StoredProcedure;

                con.Open();

                cmd.CommandText = "SP_GET_INSTALLMENT_DETAILS";
                cmd.Parameters.Add("@InstallmentID", SqlDbType.Int, 0).Value = installmentID;
                //cmd.Parameters.Add("@CustomerIDFlag", SqlDbType.Int, 0).Value = customer_id_flag;

                DataSet ds = new DataSet();


                using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                {
                    da.Fill(ds);

                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        // Binding the Grid with the Itemsource property
                        dg.ItemsSource = ds.Tables[0].DefaultView;
                    }

                }

            }
        }

        private void btn_ADD_NEW_PAYMENT_INSTALLMENT_DETAILS_Click(object sender, RoutedEventArgs e)
        {
             string result = validateAddNewInstallmentDetails();
             if (!result.Equals("true"))
                 MessageBox.Show(result);
             else if (Convert.ToInt32(tb_ADD_NEW_PAYMENT_INSTALLMENT_DETAILS.Text.Trim()) > Convert.ToInt32(remainder))
             {
                 MessageBox.Show("!خطأ .. قيمة الدفعة اكبر من الباقي");

             }
             else{
                 addNewInstallmentDetails(tb_CUSTOMER_ID_INSTALLMENT_DETAILS.Text.Trim(), tb_ADD_NEW_PAYMENT_INSTALLMENT_DETAILS.Text.Trim());

                 var newMyWindow2 = new Window1(tb_CUSTOMER_ID_INSTALLMENT_DETAILS.Text.Trim(), tb_CUSTOMER_NAME_INSTALLMENT_DETAILS.Text.Trim(), installmentID,(Convert.ToInt32(remainder) - Convert.ToInt32(tb_ADD_NEW_PAYMENT_INSTALLMENT_DETAILS.Text.Trim())).ToString());
                 //var newMyWindow2 = new Window1((row.Field<Int32>("INSTALLMENT_ID")).ToString());
                 newMyWindow2.Show();

                 this.Close();
             }
        }

        public string validateAddNewInstallmentDetails()
        {
            if (tb_CUSTOMER_ID_INSTALLMENT_DETAILS.Text.Trim() == "")
                return "!خطأ .. كود العميل";
            else if (!checkNumber(tb_CUSTOMER_ID_INSTALLMENT_DETAILS.Text.Trim()))
                return "!خطأ .. كود عميل غير صحيح";
            else if (tb_CUSTOMER_NAME_INSTALLMENT_DETAILS.Text.Trim() == "")
                return "!خطأ .. إسم العميل";
            else if (tb_ADD_NEW_PAYMENT_INSTALLMENT_DETAILS.Text.Trim() == "")
                return "!خطأ .. من فضلك ادخل الدفعة";
            else if (!checkNumber(tb_ADD_NEW_PAYMENT_INSTALLMENT_DETAILS.Text.Trim()))
                return "!خطأ .. من فضلك ادخل رقم صحيح في الدفعة";
            else
                return "true";
        }

        private static bool checkNumber(string text)
        {
            Regex regex = new Regex("[^0-9.-]+"); //regex that matches disallowed text
            return !regex.IsMatch(text);
        }


        public void addNewInstallmentDetails(string customer_id, string payment)
        {
            //  SqlConnection conn = new SqlConnection("Data Source=.\\SQLEXPRESS;AttachDbFilename=C:\\Users\\DELL\\Documents\\newd.mdf;Integrated Security=True;Connect Timeout=30;User Instance=True");
            string ConString = ConfigurationManager.ConnectionStrings["ConString"].ConnectionString;
            string CmdString = string.Empty;
            using (SqlConnection con = new SqlConnection(ConString))
            {
                SqlCommand cmd = con.CreateCommand();
                cmd.CommandType = CommandType.StoredProcedure;

                con.Open();

                cmd.CommandText = "SP_ADD_NEW_INTALLMENT_DETAILS";
                cmd.Parameters.Add("@Customer_ID", SqlDbType.Int, 0).Value = customer_id;
                cmd.Parameters.Add("@Installment_ID", SqlDbType.Int, 0).Value = installmentID;
                cmd.Parameters.Add("@Payment", SqlDbType.Int, 0).Value = payment;
               // cmd.Parameters.Add("@Initial_Payment", SqlDbType.Int, 0).Value = initial_payment;
                cmd.Parameters.Add("@Datee", SqlDbType.NVarChar, 0).Value = DateTime.Now.ToString("yyyy-MM-dd");


                cmd.ExecuteNonQuery();


            }
        }
    }
}
