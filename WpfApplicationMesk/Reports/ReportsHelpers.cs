﻿using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace MAIN.Reports
{
    public static class ReportsHelpers
    {
        public static DataSet GetDataSet<T>(IEnumerable<T> list, string tableName)
        {
            DataSet ds = new DataSet();

            DataTable table = CreateDataTable<T>(list, tableName);

            ds.Tables.Add(table);

            return ds;
        }


        public static DataTable CreateDataTable<T>(IEnumerable<T> list, string tableName)
        {
            Type type = typeof(T);
            var properties = type.GetProperties();

            DataTable dataTable = new DataTable();
            foreach (PropertyInfo info in properties)
            {
                dataTable.Columns.Add(new DataColumn(info.Name, info.PropertyType));
            }

            foreach (T entity in list)
            {
                object[] values = new object[properties.Length];
                for (int i = 0; i < properties.Length; i++)
                {
                    values[i] = properties[i].GetValue(entity, null);
                }

                dataTable.Rows.Add(values);
            }

            dataTable.TableName = tableName;

            return dataTable;
        }

        public static ReportParameter CreateReportParameter(string name, string value)
        {
            ReportParameter parameter = new ReportParameter();
            parameter.Name = name;
            parameter.Values.Add(value);
            return parameter;
        }
    }
}
