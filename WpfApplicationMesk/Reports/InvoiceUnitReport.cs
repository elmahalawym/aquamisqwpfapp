﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAIN.Reports
{
    public class InvoiceUnitReport
    {
        public string Item { get; set; }
        public int NumberOfItems { get; set; }
        public int Amount { get; set; }
    }
}
