﻿using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace MAIN.Reports
{
    /// <summary>
    /// Interaction logic for ReportsWindow.xaml
    /// </summary>
    public partial class ReportsWindow : Window
    {
        public ReportsWindow(string reportEmbeddedSource, ReportDataSource reportDataSource = null, List<ReportParameter> parameters = null)
        {
            InitializeComponent();

            reportViewer.ProcessingMode = ProcessingMode.Local;
            reportViewer.LocalReport.ReportEmbeddedResource = reportEmbeddedSource;

            reportViewer.LocalReport.DataSources.Clear();
            if (reportDataSource != null) 
                reportViewer.LocalReport.DataSources.Add(reportDataSource);

            if(parameters != null)
                reportViewer.LocalReport.SetParameters(parameters);

            reportViewer.LocalReport.Refresh();
            reportViewer.RefreshReport();
        }
    }
}
