﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAIN.Reports
{
    public class CustomerReport
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Mobile { get; set; }
        public string Telephone { get; set; }

        public string Address { get; set; }

        public int MaintenanceInterval { get; set; }

    }
}
