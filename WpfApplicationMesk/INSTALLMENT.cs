﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System.Text.RegularExpressions;

namespace MAIN
{
    class INSTALLMENT
    {
        public void getInstallment(string customer_id_val, int customer_id_flag, DataGrid dg)
        {
            string ConString = ConfigurationManager.ConnectionStrings["ConString"].ConnectionString;
            string CmdString = string.Empty;
            using (SqlConnection con = new SqlConnection(ConString))
            {
                SqlCommand cmd = con.CreateCommand();
                cmd.CommandType = CommandType.StoredProcedure;

                con.Open();

                cmd.CommandText = "SP_GET_INSTALLMENT";
                cmd.Parameters.Add("@CustomerID", SqlDbType.Int, 0).Value = customer_id_val;
                cmd.Parameters.Add("@CustomerIDFlag", SqlDbType.Int, 0).Value = customer_id_flag;
             
                DataSet ds = new DataSet();


                using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                {
                    da.Fill(ds);

                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        // Binding the Grid with the Itemsource property
                        dg.ItemsSource = ds.Tables[0].DefaultView;
                    }

                }

            }
        }



        public string getCustomerNameFromId(string customerID)
        {
            if (!checkNumber(customerID))
                return "";
            //  SqlConnection conn = new SqlConnection("Data Source=.\\SQLEXPRESS;AttachDbFilename=C:\\Users\\DELL\\Documents\\newd.mdf;Integrated Security=True;Connect Timeout=30;User Instance=True");
            string ConString = ConfigurationManager.ConnectionStrings["ConString"].ConnectionString;
            string CmdString = string.Empty;
            using (SqlConnection con = new SqlConnection(ConString))
            {
                // ComboBoxItem typeItem = (ComboBoxItem)cb_SUBREGION_ADD_CUSTOMER.SelectedItem;
                //  string value = cb_SUBREGION_ADD_CUSTOMER.Text.ToString().Trim();

                CmdString = "SELECT * FROM CUSTOMER WHERE CUSTOMER_ID = " + customerID;
                SqlCommand cmd = new SqlCommand(CmdString, con);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();

                DataTable dt = new DataTable();

                da.Fill(ds, "CUSTOMER");

                dt = ds.Tables["CUSTOMER"];
               // comboBoxName.ItemsSource = ds.Tables[0].DefaultView;
                foreach (DataRow dr in dt.Rows)
                     return (dt.Rows[0]["FULL_NAME"].ToString());
                return "";
               // comboBoxName.SelectedValuePath = ds.Tables[0].Columns["ADDRESS_ID"].ToString();

            }
        }


        public void addNewInstallment(string customer_id, string customer_name, string total, string initial_payment)
        {
            //  SqlConnection conn = new SqlConnection("Data Source=.\\SQLEXPRESS;AttachDbFilename=C:\\Users\\DELL\\Documents\\newd.mdf;Integrated Security=True;Connect Timeout=30;User Instance=True");
            string ConString = ConfigurationManager.ConnectionStrings["ConString"].ConnectionString;
            string CmdString = string.Empty;
            using (SqlConnection con = new SqlConnection(ConString))
            {
                SqlCommand cmd = con.CreateCommand();
                cmd.CommandType = CommandType.StoredProcedure;

                con.Open();

                cmd.CommandText = "SP_ADD_NEW_INTALLMENT";
                cmd.Parameters.Add("@Customer_ID", SqlDbType.Int, 0).Value = customer_id;
                cmd.Parameters.Add("@Customer_Name", SqlDbType.NVarChar, 0).Value = customer_name;
                cmd.Parameters.Add("@Total", SqlDbType.Int, 0).Value = total;
                cmd.Parameters.Add("@Initial_Payment", SqlDbType.Int, 0).Value = initial_payment;
                cmd.Parameters.Add("@Datee", SqlDbType.NVarChar, 0).Value = DateTime.Now.ToString("yyyy-MM-dd");
                

                cmd.ExecuteNonQuery();


            }
        }

        private static bool checkNumber(string text)
        {
            Regex regex = new Regex("[^0-9.-]+"); //regex that matches disallowed text
            return !regex.IsMatch(text);
        }


        public int checkExistInstallment(string customerID)
        {

            int mNumberOfRows = 0;
            string ConString = ConfigurationManager.ConnectionStrings["ConString"].ConnectionString;
            string CmdString = string.Empty;
            using (SqlConnection con = new SqlConnection(ConString))
            {
                con.Open();
                CmdString = "SELECT COUNT(*) FROM INSTALLMENT WHERE CUSTOMER_ID = " + customerID;
                // CmdString ="INSERT INTO ADDRESS(NAME, TYPE, PARENT)VALUES        (N'تااتت', 1, 5)";
                SqlCommand cmd = new SqlCommand(CmdString, con);

                //  cmd.ExecuteNonQuery();

                mNumberOfRows = (int)cmd.ExecuteScalar();
            }

            return mNumberOfRows;
        }
    }
}
