﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System.Windows.Controls;

namespace MAIN
{
    class ADDRESS
    {
        public bool addNewMainREGION(string name)
        {

            int xx = checkaddNewMainREGION(name);
            if (xx == 0)
            {
                string ConString = ConfigurationManager.ConnectionStrings["ConString"].ConnectionString;
                string CmdString = string.Empty;
                using (SqlConnection con = new SqlConnection(ConString))
                {
                    con.Open();
                    CmdString = "insert into ADDRESS (NAME, TYPE) values (N'" + name + "' , " + "3" + ")";
                    // CmdString ="INSERT INTO ADDRESS(NAME, TYPE, PARENT)VALUES        (N'تااتت', 1, 5)";
                    SqlCommand cmd = new SqlCommand(CmdString, con);

                    cmd.ExecuteNonQuery();
                }
                return true;
            }
            else
                return false;
        }
        public int checkaddNewMainREGION(string name)
        {
            int mNumberOfRows = 0;
            string ConString = ConfigurationManager.ConnectionStrings["ConString"].ConnectionString;
            string CmdString = string.Empty;
            using (SqlConnection con = new SqlConnection(ConString))
            {
                con.Open();
                CmdString = "SELECT COUNT(*) FROM ADDRESS WHERE TYPE = 3 AND NAME = N'" + name + "'";
                // CmdString ="INSERT INTO ADDRESS(NAME, TYPE, PARENT)VALUES        (N'تااتت', 1, 5)";
                SqlCommand cmd = new SqlCommand(CmdString, con);

              //  cmd.ExecuteNonQuery();

                mNumberOfRows = (int)cmd.ExecuteScalar();
            }

            return mNumberOfRows;
        }

        public void Bind_cb_REGION_WITH_TYPE(ComboBox comboBoxName,int type)
        {
            //  SqlConnection conn = new SqlConnection("Data Source=.\\SQLEXPRESS;AttachDbFilename=C:\\Users\\DELL\\Documents\\newd.mdf;Integrated Security=True;Connect Timeout=30;User Instance=True");
            string ConString = ConfigurationManager.ConnectionStrings["ConString"].ConnectionString;
            string CmdString = string.Empty;
            using (SqlConnection con = new SqlConnection(ConString))
            {
                // ComboBoxItem typeItem = (ComboBoxItem)cb_SUBREGION_ADD_CUSTOMER.SelectedItem;
                //  string value = cb_SUBREGION_ADD_CUSTOMER.Text.ToString().Trim();

                CmdString = "SELECT ADDRESS_ID,NAME FROM ADDRESS WHERE TYPE = "+type+"";
                SqlCommand cmd = new SqlCommand(CmdString, con);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds, "ADDRESS");
                comboBoxName.ItemsSource = ds.Tables[0].DefaultView;
                comboBoxName.DisplayMemberPath = ds.Tables[0].Columns["NAME"].ToString();
                comboBoxName.SelectedValuePath = ds.Tables[0].Columns["ADDRESS_ID"].ToString();

            }
        }

        public int deleteREGIONWithType(int MainRegionId, int type)
        {
       
            int mNumberOfRows = 0;
            string ConString = ConfigurationManager.ConnectionStrings["ConString"].ConnectionString;
            string CmdString = string.Empty;
            using (SqlConnection con = new SqlConnection(ConString))
            {
                con.Open();
                CmdString = "DELETE FROM ADDRESS WHERE TYPE = "+type+" AND ADDRESS_ID = " + MainRegionId + "";
                SqlCommand cmd = new SqlCommand(CmdString, con);

               mNumberOfRows =  cmd.ExecuteNonQuery();

               // mNumberOfRows = (int)cmd.ExecuteScalar();
            }

            return mNumberOfRows;
        }


        public int checkDeleteREGIONWithParent(int ParentRegionId)
        {

            int mNumberOfRows = 0;
            string ConString = ConfigurationManager.ConnectionStrings["ConString"].ConnectionString;
            string CmdString = string.Empty;
            using (SqlConnection con = new SqlConnection(ConString))
            {
                con.Open();
                CmdString = "SELECT COUNT(*) FROM ADDRESS WHERE PARENT = " + ParentRegionId;
                // CmdString ="INSERT INTO ADDRESS(NAME, TYPE, PARENT)VALUES        (N'تااتت', 1, 5)";
                SqlCommand cmd = new SqlCommand(CmdString, con);

                //  cmd.ExecuteNonQuery();

                mNumberOfRows = (int)cmd.ExecuteScalar();
            }

            return mNumberOfRows;
        }

 //-------------------------------------------------------------------------------------       
        public void loadMainInSubRegionAddress(ComboBox comboBoxName)
        {
            string ConString = ConfigurationManager.ConnectionStrings["ConString"].ConnectionString;
            string CmdString = string.Empty;
            using (SqlConnection con = new SqlConnection(ConString))
            {
                SqlCommand cmd = con.CreateCommand();
                cmd.CommandType = CommandType.StoredProcedure;

                con.Open();

                cmd.CommandText = "SP_GET_ADDRESSES_FROM_TYPE";
                cmd.Parameters.Add("@Type", SqlDbType.Int, 0).Value = 3;
                DataSet ds = new DataSet();


                using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                {
                    da.Fill(ds);
                    comboBoxName.ItemsSource = ds.Tables[0].DefaultView;
                    comboBoxName.DisplayMemberPath = ds.Tables[0].Columns["NAME"].ToString();
                    comboBoxName.SelectedValuePath = ds.Tables[0].Columns["ID"].ToString();
                }
                
            } 
        }

        public bool addNewSubREGION(string name,int parent)
        {

            int xx = checkaddNewSubREGION(name,parent);
            if (xx == 0)
            {
                string ConString = ConfigurationManager.ConnectionStrings["ConString"].ConnectionString;
                string CmdString = string.Empty;
                using (SqlConnection con = new SqlConnection(ConString))
                {
                    con.Open();
                    CmdString = "insert into ADDRESS (NAME, TYPE, PARENT) values (N'" + name + "' ,2, " + parent + ")";
                    // CmdString ="INSERT INTO ADDRESS(NAME, TYPE, PARENT)VALUES        (N'تااتت', 1, 5)";
                    SqlCommand cmd = new SqlCommand(CmdString, con);

                    cmd.ExecuteNonQuery();
                }
                return true;
            }
            else
                return false;
        }
        public int checkaddNewSubREGION(string name, int parent)
        {
            int mNumberOfRows = 0;
            string ConString = ConfigurationManager.ConnectionStrings["ConString"].ConnectionString;
            string CmdString = string.Empty;
            using (SqlConnection con = new SqlConnection(ConString))
            {
                con.Open();
                CmdString = "SELECT COUNT(*) FROM ADDRESS WHERE TYPE = 2 AND PARENT = " + parent + " AND NAME = N'" + name + "'";
                // CmdString ="INSERT INTO ADDRESS(NAME, TYPE, PARENT)VALUES        (N'تااتت', 1, 5)";
                SqlCommand cmd = new SqlCommand(CmdString, con);

                //  cmd.ExecuteNonQuery();

                mNumberOfRows = (int)cmd.ExecuteScalar();
            }

            return mNumberOfRows;
        }

        //-----/----------
        public void Bind_cb_REGION_WITH_PARENT(ComboBox comboBoxName, int parent)
        {
            //  SqlConnection conn = new SqlConnection("Data Source=.\\SQLEXPRESS;AttachDbFilename=C:\\Users\\DELL\\Documents\\newd.mdf;Integrated Security=True;Connect Timeout=30;User Instance=True");
            string ConString = ConfigurationManager.ConnectionStrings["ConString"].ConnectionString;
            string CmdString = string.Empty;
            using (SqlConnection con = new SqlConnection(ConString))
            {
                SqlCommand cmd = con.CreateCommand();
                cmd.CommandType = CommandType.StoredProcedure;

                con.Open();

                cmd.CommandText = "SP_GET_ADDRESSES_FROM_PARENT";
                cmd.Parameters.Add("@Parent", SqlDbType.Int, 0).Value = parent;
                DataSet ds = new DataSet();


                using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                {
                    da.Fill(ds);
                    comboBoxName.ItemsSource = ds.Tables[0].DefaultView;
                    comboBoxName.DisplayMemberPath = ds.Tables[0].Columns["NAME"].ToString();
                    comboBoxName.SelectedValuePath = ds.Tables[0].Columns["ID"].ToString();
                }

            }
        }



        public bool addNewStreet(string name, int parent)
        {

            int xx = checkaddNewStreet(name, parent);
            if (xx == 0)
            {
                string ConString = ConfigurationManager.ConnectionStrings["ConString"].ConnectionString;
                string CmdString = string.Empty;
                using (SqlConnection con = new SqlConnection(ConString))
                {
                    con.Open();
                    CmdString = "insert into ADDRESS (NAME, TYPE, PARENT) values (N'" + name + "' ,1, " + parent + ")";
                    // CmdString ="INSERT INTO ADDRESS(NAME, TYPE, PARENT)VALUES        (N'تااتت', 1, 5)";
                    SqlCommand cmd = new SqlCommand(CmdString, con);

                    cmd.ExecuteNonQuery();
                }
                return true;
            }
            else
                return false;
        }

        public int checkaddNewStreet(string name, int parent)
        {
            int mNumberOfRows = 0;
            string ConString = ConfigurationManager.ConnectionStrings["ConString"].ConnectionString;
            string CmdString = string.Empty;
            using (SqlConnection con = new SqlConnection(ConString))
            {
                con.Open();
                CmdString = "SELECT COUNT(*) FROM ADDRESS WHERE TYPE = 1 AND PARENT = " + parent + " AND NAME = N'" + name + "'";
                // CmdString ="INSERT INTO ADDRESS(NAME, TYPE, PARENT)VALUES        (N'تااتت', 1, 5)";
                SqlCommand cmd = new SqlCommand(CmdString, con);

                //  cmd.ExecuteNonQuery();

                mNumberOfRows = (int)cmd.ExecuteScalar();
            }

            return mNumberOfRows;
        }

    }
}
