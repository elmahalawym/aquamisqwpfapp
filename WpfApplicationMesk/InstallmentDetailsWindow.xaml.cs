﻿using MAIN.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace MAIN
{
    /// <summary>
    /// Interaction logic for InstallmentDetailsWindow.xaml
    /// </summary>
    public partial class InstallmentDetailsWindow : Window
    {
        public InstallmentDetailsWindow(int installmentId)
        {
            InitializeComponent();

            LayoutRoot.DataContext = new InstallmentDetailsViewModel(installmentId);
        }

        private void addBtn_Click(object sender, RoutedEventArgs e)
        {
            InstallmentDetailsViewModel vm = LayoutRoot.DataContext as InstallmentDetailsViewModel;

            string err;
            if(vm.AddNewUnit(out err))
            {
                MessageBox.Show("تم الإضافة بنجاح");
                this.Close();
            }
            else
            {
                if (string.IsNullOrEmpty(err))
                    MessageBox.Show("حدث خطأ غير مقصود");
                else
                    MessageBox.Show(err);
            }
        }
    }
}
